<style type="text/css">

</style>
<section class="bgAllTitles2" style="margin: -6px 0px 0px;">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle">ИДЕАЛЬНЫЙ</h1>
		<h1 class="cBlue inlineTable textTitle" style="margin: 0px -6px 0px 6px;">ДИЗАЙН-ПРОЕКТ ЭТО</h1>
	</section>
</section>
<section class="designBG allBg">
	<section class="container allContainerBg headerContBg">
		<section class="row">
			<section class="span6" style="margin:73px 0 0 18px;">
				<section class="oneText">
					01/ НЕСКОЛЬКО ВАРИАНТОВ
				</section>
				<section class="twoText">
					Предоставляется выбор из нескольких вариантов планировок.
				</section>
				<section class="oneText">
					02/ ПОЭТАПНАЯ ОПЛАТА
				</section>
				<section class="twoText" style="margin: 13px 0 77px 2px;">
					Оплата проекта производится в три этапа. По договору стоимость работ не меняется.
				</section>
				<section class="oneText">
					03/ ПОДРОБНЫЙ ПРОЕКТ
				</section>
				<section class="twoText" style="margin-left: 1px;">
					Максимально детелизированные чертежи, минимум вопросов у строителей.
				</section>
			</section>
			<section class="span6" style="margin:74px 0 0 21px;">
				<section class="oneTextR">
					04/ СНИЖЕНИЕ РАСХОДОВ
				</section><br>
				<section class="twoTextR">
					Чистовые материалы указаны в проекте и не меняются в процессе строительных работ.
				</section>
				<div style="clear:both"></div>
				<section class="oneTextR">
					05/ ЭКОНОМИЯ ВРЕМЕНИ
				</section><br>
				<section class="twoTextR" style="margin: 10px 0 60px;">
					Максимальная подробность проекта позволит строителям составить график проведения работ и график поставки материалов.
				</section><br>
				<section class="oneTextR">
					06/ УВЕРЕННОСТЬ В КАЧЕСТВЕ
				</section><br>
				<section class="twoTextR">
					Согласование каждого этапа работ<br> избавляет от сюрпризов при сдаче проекта.
				</section>
			</section>
		</section>
		<p class="btnSup">
			ХОЧУ ИДЕАЛЬНЫЙ ДИЗАЙН
		</p>
		<section class="textCenterD">
			Мы представим уровень комфорта и уюта, который Вы заслуживаете, и поможем сделать Ваш дом по-настоящему незабываемым.
		</section>
	</section>

</section>