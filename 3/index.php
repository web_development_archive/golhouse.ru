<!DOCTYPE html>
<html>
<head lang="ru">
  <title>Rastr</title>
  <meta charset="utf-8">
  <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="js/jquery.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/jquery-1.8.2.min.js"></script>
  <script src="js/countdown.js"></script>
  <script src="js/countdown_config.js"></script>
  <script src="js/countdown_config2.js"></script>
  <script src="js/countdown_config3.js"></script>
  <link rel="stylesheet" href="css/style.css"/>
  <link href="css/main.css" rel="stylesheet">

</head> 
<body>
  <section class="container" style="width:1355px;">

    <?php
    require 'pages/1header.php' ;
    require 'pages/2tasks.php';
    require 'pages/3design.php';
    require 'pages/4benefits.php';
    require 'pages/5fakts.php';
    require 'pages/6centr.php';
    require 'pages/7prof.php';
    require 'pages/8gallery.php';
    require 'pages/9demand.php';
    require 'pages/10works.php';
    require 'pages/11give.php';
    require 'pages/12about.php';
    require 'pages/13just.php';
    require 'pages/14contacts.php';
    require 'pages/15zakaz.php';

    ?>

  </section>

  <script src="js/bootstrap.min.js"></script>
</body>
</html>