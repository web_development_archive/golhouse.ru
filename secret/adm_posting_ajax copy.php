<?php
	
	include_once "db.php";
    
    //error_reporting(E_ALL);
    //ini_set('display_errors', 1);
    
	$action = $_REQUEST['action'];
	$mail=$_REQUEST['email'];
	$last = $_REQUEST['last'];
	
	if ($action=='status') {
		echo getStat();
	} else if ($action=='perform'){
		// Проверяет было ли отправлено письмо на этот email
		$check = q("SELECT m.* FROM mail_queue m JOIN emails e ON (e.id=m.email_id) WHERE e.email = :mail", array('mail' => $mail));
		
		if($check == null) sendMail($mail);
		echo getStat();
	}

	function getStat()
	{
		global $last; // переменная показывает, что последний ли этот email или нет
		$result = qCount("select count(*) from mail_queue", null);
		
		//$row = mysql_fetch_array($result);
		
		$sentcnt = $result[0];
		
		$result = qCount("select count(*) from emails where active = 1", null);
		$totalcnt = $result[0];
		
		if ($totalcnt==0) { 
			echo 100; 
			return 0; 
		}
		
		// если mail последний, то очищаем таблицу mail_queue
		if($last) q2("truncate mail_queue", null);
		
		return ($sentcnt/$totalcnt)*100;
	}

//emails - id,name, email
//mail_queue - email_id
	function sendMail($mail) {
		//global $last;
		
		$result=q("select a.id from emails a where email=:mail and active = 1",array('mail' => $mail));
		
		$row = $result[0];
		
		$id = $row['id'];
		
		$to = $mail;
		$subject = "Еженедельная рассылка Jewellclub";
		
		$body = get_body($mail);
		
		if(send_phpmail($to,$subject,$body)) {
			q2("insert into mail_queue(email_id) values(:id)", array('id' => $id));
        }
        
		
		//if($last) q2("truncate mail_queue");
	}

	function send_mail($to, $subject, $message){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		// Дополнительные заголовки
		//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
		$headers .= 'From: Jewellclub.ru <info@jewellclub.ru>' . "\r\n";
		
		// Отправляем
		return mail($to, $subject, $message, $headers);
	}

	// Функция рассылки писем
	function send_phpmail($to, $subject, $body, $file_path = null, $file_name = null){

		include_once "PHPMailermaster/PHPMailerAutoload.php";
	
		$email = new PHPMailer();
        $email->CharSet = 'UTF-8';
		$email->From      = 'info@jewellclub.ru';
		$email->FromName  = 'Jewellclub.ru';
		$email->Subject   = $subject;
		//$email->Body      = $body;
        $email->MsgHTML($body);
		$email->AddAddress($to);
        //$email->SMTPDebug  = 1;
		
		if($file_path != null){
			$file_to_attach = $file_path;
			$email->AddAttachment( $file_to_attach , $file_name );
		}

		//return $email->Send();
        if($email->Send())
            return 1;
        else return 0;
	}
	
	function get_body($mail){
	
		$hash = $mail;
	
		$body = <<<HTML
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style type="text/css">
 html, body, div, ul, li, p, h1, h2, h3, h4 {padding:0;margin:0}
</style>
</head>
<body>

<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr style="background-color:#4f4f4f;height:90px;margin:0;padding:0">
	<td><img src="http://e.rtserver.ru/jewell/images/logo.png" /></td>
	<td style="text-align:right;color:#fff;padding-right:20px;font-size:34px;font-family:Helvetica">АФИША</td>
</tr>
<tr style="background:#fff;font-family:Arial;font-size:13px;">
	<td colspan="2">
		<table border="0" cellpadding="4" cellspacing="4" width="660">
			<tr>
				<td valign="top" style="padding:20px;line-height:20px;">
					<h4 style="font-size:18px;font-family:Helvetica">Shabes Show Сезон 2 Серия 4</h4>
						<p>
Следующий выпуск: 30 января в 19:40<br />
В новой серии: Позволивший евреям покинуть Египет фараон,<br />
опомнившись, гонится за ними, чтобы силой их вернуть! Евреи обнаруживают себя запертыми между морем и армией фараона.<br />
Спасет ли свой народ Моше:? Какие еще чудеса приготовил Всев-ий для евреев:?Это и многое другое в следующих сериях нашего шаббатнего сериала! <br />
Программа вечера:<br />
19:40 — Встреча гостей на 7 этаже <br />
20:00 — Молитва <br />
20:20 — Рассадка гостей согласно указанным местам<br />
20:30 — 1-ый торжественный «ЛЕХАИМ» , посвященный недельной главе<br />
20:50 — Светские беседы, сопровождаемые приятной трапезой <br />
21:00 — 2-ой «ЛЕХАИМ» от новичка клуба <br />
21:30 — Смена блюд<br />
21:45 — Интерактивное «Shabes Show» <br />
22:00 — 3-ий «ЛЕХАИМ» от постояльца клуба <br />
22:30 — Неформальное общение , игры <br />
МЕОЦ, «Jewell club» 7 этаж, 2-ой Вышеславцев пер., 5а
						</p>
	                    <p style="margin-bottom: 20px;"></p>
	                    <a href="http://jewellclub.ru/event/shabes-show-s2-e4/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
				</td>
				<td><img src="http://jewellclub.ru/wp-content/uploads/2015/01/rUkLYiTtvNA-540x768.jpg" width="285" /></td>
			</tr>

			<tr>
				<td><img src="http://jewellclub.ru/wp-content/uploads/2015/01/nquTJoD2b-I-540x794.jpg" width="285" /></td>
				<td valign="top" style="padding:20px;line-height:20px;;padding-left:0">
					<h4 style="font-size:18px;font-family:Helvetica;">Havdala «Возвращение Легенды»</h4>
					<p>
					P — Клуб «Jewell» 7 этаж МЕОЦ<br />
A — Havdala «Возвращение Легенды»<br />
R — http://jewellclub.ru<br />
T — 31 января в 21:00<br />
Y — И все-таки это случилось! Мы возобновляем работу нашего кафе именно на Havdala party!и в этот вечер вас ожидает:<br />
— новое меню<br />
— новинки бара<br />
— новый дизайн барной стойки <br />
А делать это все мы предлагаем вам под музыку группы black sax band. Музыканты играют в стиле smooth jazz и исполняют как собственные, так и мировые хиты.<br />
							</p>
					                    <p style="margin-bottom: 60px;"></p>

	                <a href="http://jewellclub.ru/event/havdala-legendback/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>

				</td>
			</tr>
			
			<tr>
				<td valign="top" style="padding:20px;line-height:20px;">
					<h4 style="font-size:18px;font-family:Helvetica">Киносалон «Jewie-Movie»</h4>
							<p>
Каждое воскресенье в 17:00 в клубе JEWELL Вас ждут лучшие фильмы всех времен и народов, интересные обсуждения, встречи с режиссерами и приятная дружеская атмосфера. Ну и конечно, в лучших традициях кинотеатра — вкусный попкорн, газировка и сладкая вата!<br /> 
ВНИМАНИЕ <br />
Третий фильм, который мы предложим вам посмотреть называется "Поезд жизни" - это кинофильм режиссера Раду Михайляну, вышедший на экраны в 1998 году и взявший два приза Венецианского кинофестиваля! <br />
Уже в это воскресенье в 17:00 ждем тебя в клубе JEWELL<br />
							</p>
	                    <p style="margin-bottom: 20px;"></p>
	                    <a href="http://jewellclub.ru/event/киносалон-jewie-movie/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
				</td>
				<td><img src="http://jewellclub.ru/wp-content/uploads/2015/01/aAk7tC5yCj0-540x760.jpg" width="285" /></td>
			</tr>
			<tr>
				<td valign="top" style="padding-right:0;"><img src="http://jewellclub.ru/wp-content/uploads/2015/01/SccYHAv2F24-540x758.jpg" width="285" /></td>
				<td valign="top" style="padding:20px;line-height:20px;padding-left:0">
					<h4 style="font-size:18px;font-family:Helvetica">«ОранJEWрея»</h4>
			
							<p>
					Посреди серой зимы мы приглашаем тебя окунуться в мир настоящих тропиков Ботанического сада , чтобы отпраздновать Новый год деревьев.<br />
- Богатейшая коллекция растений, собранных со всего мира!<br />
- Необычные мастер-классы<br />
Ну и конечно же наслаждайся прекрасным вечером с кальянами, вином и разными вкусностями!<br />
Благотворительный взнос за мероприятие 300 рублей заранее, 500 в день. Всего 100 мест<br />
От метро ВДНХ будет курсировать маршрутка, на которой вы сможете добраться до места назначения!<br />
					</p>
			
	                    <p style="margin-bottom: 20px;"></p>

	                <a href="http://jewellclub.ru/event/оранjewрея/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>

				</td>
			</tr>
			
			<tr>

				<td valign="top" style="padding:20px;line-height:20px;">
					<h4 style="font-size:18px;font-family:Helvetica">Третий Пуримский Кино Фестиваль «ЭСТИ»</h4>

								<p>
						4 марта в 19:00<br />
Третий Пуримский Кино Фестиваль «ЭСТИ»<br />
Хочешь получить билеты в Израиль? — просто сними видео!<br />
Чтобы принять участие в фестивале необходимо:<br />
- Собрать команду — максимум 6 человек (в которой хотя бы 5 галахических евреев)<br />
- Подать заявку до 9 февраля, заполнив анкету ниже.<br />
- Снять короткометражный ролик максимум на 10 минут, связанный с праздником Пурим в любой интерпретации. (свободный жанр: пародия, реклама, мульт, муз. клип, переозвучка) Желательно с юмором!<br />
- Разместить видео на файл обменнике, прислать ссылку на скачивание на почту jewellclub@gmail.com не позднее 26 февраля.<br />
5 лучших фильмов будут представлены на фестивале 4-го марта профессиональному жюри.<br />
<br />
Ролики содержащие пошлость и жестокость к показу допущены не будут!!!<br />
<br />
Также в этот день вас будет ждать:<br />
- яркая шоу-программа<br />
- чтение свитка Эстер<br />
- Закрытая вечеринка для тех, кто забронирует места заранее.<br />
- Бар, фуршет, все дела…<br />
						</p>
			
	                    <p style="margin-bottom: 20px;"></p>
	                    <a href="http://jewellclub.ru/event/purim_kinofestival/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
				</td>
				<td><img src="http://jewellclub.ru/wp-content/uploads/2015/01/eX2c7K98_VA-540x762.jpg" width="285" /></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td style="background:#006cad;text-align:center;vertical-align:middle;padding:5px 0;color:#fff;font-family:Arial;font-size:14px;" colspan="2">
		<div class="register" style="display:block;margin:0;padding:0"><a style="color:white;text-decoration:none" href="http://dev3.realty-surfer.com/secret/unsubscribe.php?hash={$hash}">Отписаться от рассылки</a></div>
		<div class="register">Регистрация на мероприятия:</div>
		<div class="site"><a style="color:#fff;text-decoration:none" href="http:\\www.jewellclub.ru\">www.jewellclub.ru</a></div>
	</td>
</tr>	
</table>

</body>
</html>	
HTML;

		return $body;
	
	}


?>