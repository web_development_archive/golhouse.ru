<?php
include_once("db.php");

$emails = q("SELECT * FROM emails WHERE active = 1", null);

$emails_list = "";
$coma = "";

foreach($emails as $mail){
	$emails_list .= $coma.$mail["email"];
	$coma = ",";
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Jewell</title>
    <!-- Bootstrap -12,38  325x640-->
    <meta charset="UTF-8">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="../css/main.css">-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <style type="text/css">
  	.ui-progressbar {
    position: relative;
	  }
	  .progress-label {
	  	position: absolute;
	  	top:0;
	  	width: 100%;
	  	left:0;
	    text-align: center;
	    line-height: 30px;
	    font-weight: bold;
	    font-family: Arial;
	  }
  </style>
  </head>
  <body>
    <div class="container" style="width:660px">
    	<h2>Отправка рассылки</h2>
    	<p>Для того, чтобы отправить письма получателям, Вам нужно нажать кнопку "Send" и подождать окончания процесса.</p>
      <!--<textarea id="emails"></textarea>-->
      <button id="sendbutton" class="btn btn-default">Send</button>
      <div id="progressbar" style="margin-top:10px"><div class="progress-label">0%</div></div>
      <!--
      <div class="progress">
	  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
	    0%
	  </div>
	  -->
	  
	  <h3>Предпросмотр</h3>
      <iframe frameborder="0" src="/template.html" width="660" height="400"></iframe>
	</div>
      <input type="hidden" id="emails" value="<?php echo $emails_list ?>" />
      
    </div>
    <script type="text/javascript">
    // Удаляет все пробелы в стоке писем. (список писем текст эриа)
    
    var progressLabel = $('.progress-label');
    
      function nospace(str) {
        var VRegExp = new RegExp(/^[ ]+/g);
        var VResult = str.replace(VRegExp, '');
        return VResult
      }
      // отправка письма на данную почту
      function sendMl(email, last)
      {
      
      $.ajax({
  	url: "adm_posting_ajax.php?action=perform&email=" + email +"&last=" + last,
  	async: false
	}).done(function(data) {
	$("#progressbar").progressbar("option", {value: Math.floor(data)});
    progressLabel.text(data+'%');
   	// Todo something..
	}).fail(function(xhr)  {
   // Todo something..
	});
      
//         $.get("adm_posting_ajax.php?action=perform&email=" + email +"&last=" + last,  function(data) {
        	
        	//$('.progress-bar').attr('aria-valuenow', data).css('width', data+'%').html(data+'%');
        	
        // 	$("#progressbar").progressbar("option", {value: Math.floor(data)});
//         	progressLabel.text(data+'%');
          
//         });
      }

      $(document).ready(function() 
      {
      
      	
        $("#progressbar").progressbar({ 
        	value: 0
        });

        $.get("adm_posting_ajax.php?action=status", function(data) {
          
          $("#progressbar").progressbar( "option", {value: Math.floor(data)} );
          progressLabel.text(data+'%');
          
          //$('.progress-bar').attr('aria-valuenow', data).css('width', data+'%').html(data+'%');
          
          $("#sendbutton").click(function(e) {
          	
            e.preventDefault();
            var emails = document.getElementById("emails").value;
            emails = nospace(emails);
            var array = emails.split(',');
            
            var size = array.length;
            var last = 0;
            var timeout =0;
            var i = 0;

			function nextMail()
			{
				if (i<size)
				{
				setTimeout(nextMail(), 1000);
				}
				last = (i == (size-1)) ? 1 : 0;
				sendMl(array[i], last);
				i++;
			}
			nextMail();
			
            
            // парсим строку с почтой и по одной отправляем
            //$.each( array, function( key, value ) {
            //  sendMl(value);
            //  setTimeout(function () {},1000);
            //});
          });

        });
      });
    </script>
  </body>
</html>