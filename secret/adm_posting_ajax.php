<?php
	
	include_once "db.php";
    
    //error_reporting(E_ALL);
    //ini_set('display_errors', 1);
    
	$action = $_REQUEST['action'];
	$mail=$_REQUEST['email'];
	$last = $_REQUEST['last'];
	
	if ($action=='status') {
		echo getStat();
	} else if ($action=='perform'){
		// Проверяет было ли отправлено письмо на этот email
		$check = q("SELECT m.* FROM mail_queue m JOIN emails e ON (e.id=m.email_id) WHERE e.email = :mail", array('mail' => $mail));
		
		if($check == null) sendMail($mail);
		echo getStat();
	}

	function getStat()
	{
		global $last; // переменная показывает, что последний ли этот email или нет
		$result = qCount("select count(*) from mail_queue", null);
		
		//$row = mysql_fetch_array($result);
		
		$sentcnt = $result[0];
		
		$result = qCount("select count(*) from emails where active = 1", null);
		$totalcnt = $result[0];
		
		if ($totalcnt==0) { 
			echo 100; 
			return 0; 
		}
		
		// если mail последний, то очищаем таблицу mail_queue
		if($last) q2("truncate mail_queue", null);
		
		return ($sentcnt/$totalcnt)*100;
	}

//emails - id,name, email
//mail_queue - email_id
	function sendMail($mail) {
		//global $last;
		
		$result=q("select a.id from emails a where email=:mail and active = 1",array('mail' => $mail));
		
		$row = $result[0];
		
		$id = $row['id'];
		
		$to = $mail;
		$subject = "Еженедельная рассылка Jewellclub";
		
		$body = get_body($mail);
		
		if(send_phpmail($to,$subject,$body)) {
			q2("insert into mail_queue(email_id) values(:id)", array('id' => $id));
        }
        
		
		//if($last) q2("truncate mail_queue");
	}

	function send_mail($to, $subject, $message){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		// Дополнительные заголовки
		//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
		$headers .= 'From: Jewellclub.ru <info@jewellclub.ru>' . "\r\n";
		
		// Отправляем
		return mail($to, $subject, $message, $headers);
	}

	// Функция рассылки писем
	function send_phpmail($to, $subject, $body, $file_path = null, $file_name = null){

		include_once "PHPMailermaster/PHPMailerAutoload.php";
	
		$email = new PHPMailer();
        $email->CharSet = 'UTF-8';
		$email->From      = 'info@jewellclub.ru';
		$email->FromName  = 'Jewellclub.ru';
		$email->Subject   = $subject;
		//$email->Body      = $body;
        $email->MsgHTML($body);
		$email->AddAddress($to);
        //$email->SMTPDebug  = 1;
		
		if($file_path != null){
			$file_to_attach = $file_path;
			$email->AddAttachment( $file_to_attach , $file_name );
		}

		//return $email->Send();
        if($email->Send())
            return 1;
        else return 0;
	}
	
	function get_body($mail){
	
		$hash = md5($mail);
	
		$body = <<<HTML
<html>
<head>
<meta charset="UTF-8">
<style type="text/css">
	html, body, div, ul, li, p, h1, h2, h3, h4 {padding:0;margin:0}
	.contentBtn {
	border-radius: 5px;
	}
	.contentBtn:hover
	{
		background: #4F4F1B!important;
	}
	h4 {
	padding: 0 0 5px 0;
	}
	p {
	display: inline-block;
	}
	td {
	width: 52%;
	}
</style>
</head>
<body>

	<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr style="background-color:#4f4f4f;height:90px;margin:0;padding:0">
		<td><img src="http://e.rtserver.ru/jewell/images/logo.png" style="margin: 0 0  0 10px;"/></td>
		<td style="text-align:right;color:#fff;padding-right:30px;font-size:34px;font-family:Helvetica">АФИША</td>
	</tr>
	<tr style="background:#fff;font-family:Arial;font-size:13px;">
		<td colspan="2">
			<table border="0" cellpadding="4" cellspacing="4" width="660" style="">
				<tr>
					<td valign="top" style="padding:20px 20px 20px 0;line-height:20px;">
						<h4 style="margin: 0 0 0 5px; font-size:18px;font-family:Helvetica">"От кипы до капы" Встреча с Дмитрием Салита</h4>
							<p style="margin: 0 0 0 5px;">
«Каждому, кто хочет получить от меня хорошую трёпку, придётся подождать до заката» Именно так говорит Дмитрий Салита , когда его бой выпадает на святой для евреев день- субботу, а все потому что, кроме того, что он отличный боксер, он еще и соблюдающий заповеди еврей!<br>
Также Он не борется в святые для иудаизма дни (70 в году) и питается кошерной пищей. Он тренируется на расстоянии пешего перехода от синагоги ради пятничной и субботней службы и не водит машину в шаббат.<br>
Хотите познакомиться с ним лично, задать вопросы или просто сделать #СелфиСоЗвездой?<br>
Тогда уже завтра, 20 мая в 20:00 ждем вас в JEWELLCLUB<br>
							</p>
		                    <p style="margin-bottom: 50px;"></p>
		                    <a href="http://jewellclub.ru/event/salita_in_jewell/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
					</td>
					<td style="vertical-align: top;padding:0"><img src="http://cs627621.vk.me/v627621167/27b1/caf2W2Y53Wo.jpg" width="285" style="margin: 20px 0 0 0;" /></td>
				</tr>

				<tr>
					<td><img style="    margin: 0px 0 0 0;" src="http://cs627621.vk.me/v627621167/27a8/khW9eJ-VhWk.jpg" width="285" /></td>
					<td valign="top" style="line-height:20px;;padding-left:0">
						<h4 style="font-size:18px;font-family:Helvetica;">"Shabes Show" Сезон 4 серия 1</h4>
						<p>
Следующий выпуск: 22 мая в 19:40<br>
Вот мы закончили с Вами 3 из 5 книг Торы и перешли к новой главе, а значит и новому 4СЕЗОНУ нашего субботнего сериала "Shabes Show"<br>
В новой серии: "На первый- второй- двенадцатый , рассчитайся!" Именно этому будет наша новая серия! а именно 12 коленам Израиля! <br>
И ,конечно, по-традиции вас ждет праздничная трапеза, душевные разговоры и масса интерактива!<br>
						</p>
						<p style="margin-bottom: 60px;"></p>

		                <a href="http://jewellclub.ru/event/shabes-show-s3-e7/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>

					</td>
				</tr>

				<tr>
					<td valign="top" style="padding:20px 20px 20px 0;line-height:20px;">
						<h4 style="margin: 0 0 0 5px; font-size:18px;font-family:Helvetica">havdala "Время Торы"</h4>
							<p style="margin: 0 0 0 5px;">
23 мая вас ждет закрытие сезона вечеринок havdala - party!<br>
Эта суббота совпадает с еврейским праздником Шавуот и существует обычай учиться всю ночь на пролет!<br>
Поэтому в JEWELLCLUB объявляется время Торы! <br>
В эту ночь опытные раввины устроят вам ликбез по иудаизму и миру Торы!<br>
Не пропусти наш "маленький фарбренген" на 7 этаже!<br>
<br />
							</p>
		                    <p style="margin-bottom: 50px;"></p>
		                    <a href="http://jewellclub.ru/event/havdala_lailatora/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
					</td>
					<td style="    vertical-align: top;padding:0"><img src="http://cs627621.vk.me/v627621167/279f/ELugFuufWo8.jpg" width="285" style="margin: 20px 0 0 0;" /></td>
				</tr>
				
				<tr>
					<td><img style="    margin: 0px 0 0 0;" src="http://cs627621.vk.me/v627621167/2796/W92skr1UtQs.jpg" width="285" /></td>
					<td valign="top" style="line-height:20px;;padding-left:0">
						<h4 style="font-size:18px;font-family:Helvetica;">МИЛКИ ой ВЭЙ!</h4>
						<p>
МИЛКИ ой ВЭЙ 24 мая в 16:00<br>
В JEWELLCLUB объявляется время молочных рек и кисельных берегов :)<br>
Праздничная программа на ШАВУОТ для молодёжи!<br>
МЕОЦ - молочный ресторан<br>
- горячая десятка заповедей<br>
- деликатесная трапеза<br>
- интерактивная программа<br>
Количество мест ограничено.<br>
Поэтому успей зарегистрироваться<br>
						</p>
						<p style="margin-bottom: 60px;"></p>

		                <a href="http://jewellclub.ru/event/milkiway_jewell/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>

					</td>
				</tr>
				<tr>
				<td style="background:#006cad;text-align:center;vertical-align:middle;padding:5px 0;color:#fff;font-family:Arial;font-size:14px" colspan="2">
				<div style="display:block;margin:0;padding:0"><a style="color:white;text-decoration:none" href="http://dev3.realty-surfer.com/secret/unsubscribe.php?hash=erzhan.kst@gmail.com" target="_blank">Отписаться от рассылки</a></div>
				<div>Регистрация на мероприятия:</div>
				<div><a style="color:#fff;text-decoration:none" href="http:%5Cwww.jewellclub.ru%5C" target="_blank">www.<span class="il">jewellclub</span>.ru</a></div>
				</td>
				</tr>
				
			</table>
		</td>
			</tr>				
			</table>
</body>
</html>	
HTML;

		return $body;
	
	}


?>