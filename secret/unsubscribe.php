<?php

include_once "db.php";

$hash = $_GET['hash'];
$msg = '';

$q = q("SELECT * FROM emails WHERE MD5(email) = :hash", array('hash' => $hash));

if(count($q) == 0 || empty($hash)){
	if(q2("UPDATE emails SET active = 0 WHERE email = :hash", array('hash' => $hash))){
		$msg = 'Вы успешно отписались от рассылки';
	}	
}else $msg = 'Такой E-mail не найден в базе';

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Jewell</title>
    <!-- Bootstrap -12,38  325x640-->
    <meta charset="UTF-8">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="../css/main.css">-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  </head>
  <body>
    <div class="container" style="width:600px;margin-top:100px;text-align:center">
    	<div class="well">
    		<?php echo $msg ?>
    	</div>
    </div>
  </body>
</html>