<?php
$dbConnection = new PDO('mysql:dbname=realtysu_test;host=localhost;charset=utf8', 'realtysu_test', 'test0ABC');

$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbConnection->exec("set names utf8");

function q($sql, $params) // запрос к базе — короткое имя для удобства
{
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function q2($sql, $params){ // Используется для insert и update
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);

    if($stmt->execute($params)) return true;
    else return false;
}

function qCount($sql, $params){ // Выводит количество записей
		global $dbConnection;
		$stmt = $dbConnection->prepare($sql);
		$stmt->execute($params);
		return $stmt->fetchColumn();
}


function qInsertId(){ // Последнйи автоинкриментный ID
    global $dbConnection;
    return $dbConnection->lastInsertId();
}

define("SQL_LOGIN_ATTEMPT", "SELECT u.* FROM users u WHERE u.mail = :mail AND u.pass=MD5(:pass)");
define("SQL_CHECK_USER", "SELECT * FROM users WHERE mail = :mail");

?>