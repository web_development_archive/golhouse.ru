<?php

	include_once "db.php";
	set_time_limit(0);
    
     //error_reporting(E_ALL);
     //ini_set('display_errors', 1);

mainCycle();

function mainCycle()
{
		$result=q("select e.id, e.email from emails e where active=1 and not exists (select 1 from mail_queue where email_id=e.id)", array());
		
		for ($i=0;$i<count($result);$i++)
		{
			$row = $result[$i];
			$id = $row['id'];
			$to = $row['email'];
			$subject = "Еженедельная рассылка Jewellclub";
			$body = get_body($to);
			//echo "sent...<br/>";
			 
			$sentCount = q("select count(*) cnt from mail_queue", null);
			$totalCount = q("select count(*) cnt from emails where active=1", null);

			
			$sentCount = $sentCount[0]['cnt'];
			$totalCount = $totalCount[0]['cnt'];
			
			echo number_format(($sentCount / $totalCount * 100) , 2, '.', ''). '%'.'---';
			
			
			if(send_phpmail($to,$subject,$body)) 
			{
				q2("insert into mail_queue(email_id) values(:id)", array('id' => $id));
			}
			
			
			
       	    sleep(0.5);
        }
        
        echo "finished.";

    


}



// Функция рассылки писем
	function send_phpmail($to, $subject, $body, $file_path = null, $file_name = null){

		include_once "PHPMailermaster/PHPMailerAutoload.php";
	
		$email = new PHPMailer();
        $email->CharSet = 'UTF-8';
		$email->From      = 'info@jewellclub.ru';
		$email->FromName  = 'Jewellclub.ru';
		$email->Subject   = $subject;
		//$email->Body      = $body;
        $email->MsgHTML($body);
		$email->AddAddress($to);
        //$email->SMTPDebug  = 1;
		
		if($file_path != null){
			$file_to_attach = $file_path;
			$email->AddAttachment( $file_to_attach , $file_name );
		}

		//return $email->Send();
        if($email->Send())
            return 1;
        else 
        {
            sleep(3);
			//die("Failed to send");
        	return 0;
        }
	}



function get_body($trgt){
	
		//$hash = $mail;
	
		$body = <<<HTML
<html>
<head>
<meta charset="UTF-8">
<style type="text/css">
 html, body, div, ul, li, p, h1, h2, h3, h4 {padding:0;margin:0}
 .contentBtn {
border-radius: 5px;
}
.contentBtn:hover
{
	background: #4F4F1B!important;
}
h4 {
padding: 0 0 5px 0;
}
p {
display: inline-block;
}
td {
width: 52%;
}
</style>
</head>
<body>

	<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr style="background-color:#4f4f4f;height:90px;margin:0;padding:0">
		<td><img src="http://e.rtserver.ru/jewell/images/logo.png" style="margin: 0 0  0 10px;"/></td>
		<td style="text-align:right;color:#fff;padding-right:30px;font-size:34px;font-family:Helvetica">АФИША</td>
	</tr>
	<tr style="background:#fff;font-family:Arial;font-size:13px;">
		<td colspan="2">
			<table border="0" cellpadding="4" cellspacing="4" width="660" style="">
				<tr>
					<td valign="top" style="padding:20px 20px 20px 0;line-height:20px;">
					<h4 style="font-size:18px;font-family:Helvetica;">Выставка «Синагоги Центральной Европы»
</h4>
						<p>
						<br/> 
<b>Открытие 28 мая в 19:00</b> <br><br>
Выставка-путешествие и выставка-напоминание, через архитектуру раскрывающая историю жизни еврейской общины в Центрально-Европейском регионе.
<br/><br/>
Место проведения
Венгерский культурный центр в Москве.
Москва, Поварская улица 21						</p>
		                    <p style="margin-bottom: 50px;"></p>
		                    <a href="http://jewellclub.ru/event/sinagoga_evropa/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
					</td>
					<td style="vertical-align: top;padding:0"><img src="http://jewellclub.ru/wp-content/uploads/2015/05/poster_event_209548-540x762.png" width="285" style="margin: 20px 0 0 0;" /></td>
				</tr>

				<tr>
					<td><img style="    margin: 0px 0 0 0;" src="http://jewellclub.ru/wp-content/uploads/2015/05/7OE7IBqy39M-540x766.jpg" width="285" /></td>
					<td valign="top" style="line-height:20px;;padding-left:0">
						<h4 style="margin: 0 0 0 5px; font-size:18px;font-family:Helvetica">«Shabes Show» День рождения Берла Лазара</h4>
							<p style="margin: 0 0 0 5px;">
						<p>
Вот и вновь мы с вами получили Тору, а теперь самое время выполнить одну из важнейших заповедей, написанных в ней — шаббат.
В этот раз <b>29 мая в 19:40</b> мы отметим с вами торжественное событие, а именно – ДЕНЬ РОЖДЕНИЯ ГЛАВНОГО РАВВИНА РОССИИ – БЕРЛА ЛАЗАРА.
В течение вечера мы придумаем с вами креативное поздравление, а затем представим его раввину.
Ребята, пришедшие вовремя и принявшие участие в подготовке поздравления раввина, будут приглашены на спонсорскую трапезу в честь торжества.<br>
						</p>
						<p style="margin-bottom: 60px;"></p>
		                <a href="http://jewellclub.ru/event/drlazara_in_jewell/" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>

					</td>
				</tr>

				<tr>
					<td valign="top" style="padding:20px 20px 20px 0;line-height:20px;">
						<h4 style="margin: 0 0 0 5px; font-size:18px;font-family:Helvetica">Концерт еврейских и израильских песен</h4>
							<p style="margin: 0 0 0 5px;">
						<p>
<b>1 июня в 20:00</b> <br> 

Приглашаем вас на концерт традиционной и современной израильской музыки в исполнении коллектива «Mostov project». 

Мы хотим напомнить нашим слушателям о богатом наследии еврейского музыкального творчества и познакомить с современными израильскими песнями! Концерт пройдёт в дискотечном формате и можно будет танцевать прямо в зале)

В составе коллектива музыканты из Кубы, Армении, России, объединённые общей идеей создать интерес к еврейской и израильской музыкальной культуре через исполнение красивых и мелодичных песен... 					</p>
<br />
							</p>
		                    <p style="margin-bottom: 50px;"></p>
		                    <a href="https://vk.com/event93853570" target="_blank" class="contentBtn" style="background:#4f4f4f;padding:14px 25px;color:white;text-decoration:none;font-size:15px">Зарегистрироваться</a>
					</td>
					<td style="    vertical-align: top;padding:0"><img src="http://jewellclub.ru/wp-content/uploads/2015/05/i4B7xu-YBGg.jpg" width="285" style="margin: 20px 0 0 0;" /></td>
				</tr>
					<tr>
					<td style="background:#006cad;text-align:center;vertical-align:middle;padding:5px 0;color:#fff;font-family:Arial;font-size:14px" colspan="2">
						<div style="display:block;margin:0;padding:0"><a style="color:white;text-decoration:none" href="http://dev3.realty-surfer.com/secret/unsubscribe.php?hash=$trgt" target="_blank">Отписаться от рассылки</a></div>
						<div>Регистрация на мероприятия:</div>
						<div><a style="color:#fff;text-decoration:none" href="http://www.jewellclub.ru" target="_blank">www.<span class="il">jewellclub</span>.ru</a></div>
					</td>
				</tr>
			</table>
			<iframe width=1 height=1 src='http://dev3.realty-surfer.com/secret/read.php?mail=$trgt'>
</body>
</html>			
HTML;

		return $body;
	
	}

?>