<style type="text/css">

</style>
<section class="bgAllTitles" id="schema">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle" style="margin: 0px 8px 0px 0px;">СХЕМА </h1>
		<h1 class="cBlue inlineTable textTitle">РАБОТЫ</h1>
	</section>
</section>
<section class="worksBG allBg">
	<section class="container headerContBg" style="width:1000px;">
		<section style="margin-top:27px;"></section>
		<p class="btnSuper">
			ОСТАВЛЯЕТЕ ЗАЯВКУ
		</p>
		<section style="margin:147px 0 0 -3px;">
			<section class="inlineTable wMitty">
				<header>
					ПОЛУЧЕНИЕ ИСХОДНЫХ ДАННЫХ
				</header>
				<p>Замер помещения</p>
				<p>Ваши пожелания</p>
				<p>Заключение договора</p>
				<footer>ДЕНЬ 1</footer>
			</section>
			<section class="inlineTable wMitty" style="margin-left:127px">
				<header style="margin-bottom: 27px; width:206px;">
					РАБОТА НАД ПРОЕКТОМ
				</header>
				<p>3D визуализация</p>
				<p>Рабочие чертежи</p>
				<p>Подбор мебели и материалов</p>
				<footer>ДЕНЬ 6-21</footer>
			</section>
			<section class="inlineTable wMitty" style="margin-left:197px">
				<header>
					ДЕКОРИРОВАНИЕ ПОМЕЩЕНИЯ
				</header>
				<p>Украшение стен</p>
				<p>Наполнение аксессуарами</p>
				<p>Флористика</p>
				<footer>ДЕНЬ 30+</footer>
			</section>
		</section>
		<section style="margin-top: 87px;">
			<section class="inlineTable wMitty" style="margin-left:212px">
				<footer>ДЕНЬ 3</footer>
				<header style="margin: 23px 0 28px; width: 201px;">
					ПРЕДЛАГАЕМЫЕ ВАРИАНТЫ
				</header>
				<p>3 варианта планировки</p>
				<p>Цветовые решения</p>
				<p>Выбор стиля</p>
			</section>
			<section class="inlineTable wMitty" style="margin: 0px 0px 0px 190px; width: 355px;">
				<footer>ДЕНЬ 21+</footer>
				<header style="width: 198px; margin: 23px 0px 5px;">
					СОПРОВОЖДЕНИЕ ПРОЕКТА
				</header>
				<p style="margin: 0px 0px 4px 10px;">Авторский надзор</p>
				<p style="margin: 0px 0px -3px 10px;">Наполнение объекта (закупка мебели, материа-<br>лов, сантехники, мультимедиа, света, кухни, <br>разеток, умный дом, вентиляция)</p>
				<section class="dopUsl">
					Дополнительные услуги
				</section>
			</section>
		</section>
		<section class="botText">
			ВАШ ИНТЕРЬЕР ГОТОВ!
		</section>
		
		<p class="btnSuper2">
			ХОЧУ ТАКЖЕ
		</p>
	</section>

</section>