<style type="text/css">

</style>
<section class="bgAllTitles">
	<section style="text-align:center;">
		<h1 class="inlineTable cGray textTitle">ФАКТЫ</h1>
		<h1 class="cBlue inlineTable textTitle" style="margin: 0px -10px 0px 6px;">О НАС</h1>
	</section>
</section>
<section class="faktsBG allBg">
	<section class="container allContainerBg headerContBg">
		<section style="margin-top:64px">
			<section class="inlineTable blocks">
				<section class="numbers">76</section>
				<section class="textNiz">ЧАСТНЫХ КОТТЕДЖЕЙ</section>
			</section>
			<section class="inlineTable blocks">
				<section class="numbers">150</section>
				<section class="textNiz">ЖИЛЫХ КВАРТИР</section>
			</section>
			<section class="inlineTable blocks" style="margin: 5px 43px 20px -10px;">
				<section class="numbers">27</section>
				<section class="textNiz">АППАРТАМЕНТОВ</section>
			</section>
			<section class="inlineTable blocks"  style="margin: 5px -3px 20px -6px;">
				<section class="numbers">12</section>
				<section class="textNiz">ОФИСНЫХ ПОМЕЩЕНИЙ</section>
			</section>
			<section class="inlineTable blocks">
				<section class="numbers">2</section>
				<section class="textNiz">КЛУБА</section>
			</section>
			<section class="inlineTable blocks"  style="margin-right: 34px;">
				<section class="numbers">11</section>
				<section class="textNiz">САЛОНОВ КРАСОТЫ</section>
			</section>
			<section class="inlineTable blocks">
				<section class="numbers">3</section>
				<section class="textNiz">РЕСТОРАНА</section>
			</section>
			<section class="inlineTable blocks"  style="margin: 5px -3px 20px -6px;">
				<section class="numbers">14</section>
				<section class="textNiz">МАГАЗИНОВ</section>
			</section>
		</section>
	</section>

</section>