<style type="text/css">

</style>
<section class="bgAllTitles" id="ourworks">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle" style="margin: 5px 9px 0px -9px;">НАШИ </h1>
        <h1 class="cBlue inlineTable textTitle">РАБОТЫ</h1>
	</section>
</section>
<section class="galleryBG container">
	<!--<section class="container allContainerBg headerContBg">-->
		<?php
/**
 * Класс фото-галереи на сайт
 * @author www.webinit.ru
 */
class Gallery {

    public function getGallery() {
        //Выбираем все содержимое папки images, и записываем из в массив $files
        $files = scandir("images/"); 
        $gallery_files = array();
        foreach ($files as $key => $value) { //Проходим по массиму
            //Проверяем файл или нет, если файл, то:
            if (filetype("images/" . $value) == "file") { 
                $gallery_files[] = $value;  //Записываем в массив
            }
        }
        return $gallery_files; //Возвращаем массив
    }

}

$obj = new Gallery();
$gallery = $obj->getGallery();
?>
<img class="imgGallery" src="" alt="" id="gallery" />
<section style="text-align: center;"><div id="number_img"></div>
<a href="javascript:void(0)" class="btn" id="btnBack" onclick="fadeOutGallery(); setTimeout('backImg()', 500); this.blur();">Назад</a>
<a  class="btn" href="javascript:void(0)" id="btnGo"  onclick="fadeOutGallery(); setTimeout('nextImg()', 500); this.blur();">Вперед</a>
</section>
<script type="text/javascript">
    var images = new Array();
    var current_image_key = 0; //Переменная содержит номер текущей фотографии

<?php
foreach ($gallery as $key => $file) { //Проходим по всем фотографиям
    echo "images[$key] = new Image();\n\r"; //Создаем новый объект Images
    echo "images[$key].src = './images/$file';\n\r"; //Записываем путь к фотографии
}
?> 
/**
 * Функция обновляет текущее изображение, и его номер
 */
function refreshImage() {
    fadeInGallery();
    //Изменяем изображение на текущее
    document.getElementById("gallery").src = images[current_image_key].src;
   //Изменяем надпись под изображением
    document.getElementById("number_img").innerHTML =
        (current_image_key+1) + " из " + images.length
        
}

function fadeOutGallery()
{
    $("#gallery").fadeOut("slow");
    $("#number_img").fadeOut("slow");
    $("#btnBack").fadeOut("slow");
    $("#btnGo").fadeOut("slow");
}
function fadeInGallery()
{
    $("#gallery").fadeIn();
    $("#number_img").fadeIn();
    $("#btnBack").fadeIn();
    $("#btnGo").fadeIn();
}
/**
 * Следующая фотография
 */
function nextImg() {
    current_image_key++; //Увеличиваем текущую фотографию на 1
   //Если достигнут конец, то делаем первую фотографию текущей
    if (current_image_key >= images . length) current_image_key = 0; 
    refreshImage(); //Обновляем фотографию
}
/**
 * Предыдущая фотография
 */
function backImg() {
    current_image_key--; //Уменьшаем текущую фотографию на 1
   //Если достигнуто начало, то делаем последнюю фотографию текущей
    if (current_image_key < 0) current_image_key = images . length - 1; 
    refreshImage(); //Обновляем фотографию
}
refreshImage(); //Обновляем фотографию
</script>
	<!--</section>-->

</section>