<?php
	$post = (isset($_POST['Cons']) && !empty($_POST['Cons'])) ? true : false;
	if($post)

	{
	    $to      = 'erzhan.kst@gmail.com';
	    $subject = trim($_POST['Cons']['email']);
	    $message = htmlspecialchars($_POST['Cons']['name'])." ".htmlspecialchars($_POST['Cons']['phone'])." ".htmlspecialchars($_POST['Cons']['email']);
	    $headers = 'От: '.htmlspecialchars($_POST['Cons']['email'])."\r\n".'Reply-To: erzhan.kst@gmail.com' . "\r\n" .'X-Mailer: PHP/' . phpversion();
	    $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
	    $mail = mail($to, $subject, $message, $headers);
	}
?>
<section class="centerBg">
	<section class="allBg">
		<section class="container allContainerBg headerContBg">
			<section class="row" style="margin: -22px 0px 0px -21px;">
				<section class="span8 timerLeftCentr">
					<section style="margin: 15px 15px 0 0;">
						<h1 class="inlineTable cGray timerLeftCentr2" style="font-size:50px">СКИДКА </h1><h1 class="inlineTable cRed" style="font-family: comRegular; vertical-align: top; font-size: 72px; margin: 10px -1px 0px 1px;font-weight:bold">10%</h1>
						<section style="margin: 15px -5px 0px 0px;">
							<h3 style="font-size: 24px; font-family: conRegular; margin:0;font-weight:bold" class="inlineBlockI cGray">НА</h3>
							<h3 style="margin:0;font-size: 48px; font-family: conRegular; margin: 0px -6px;font-weight:bold" class="cBlue inlineBlockI">ПЕРВЫЙ</h3>
							<h3 style="font-size: 24px; font-family: conRegular; margin:0; font-weight: bold;" class="cGray inlineBlockI">ЗАКАЗ</h3>
						</section>
						<h3 style="margin:-11px 0 0 0; font-size: 24px; font-family: conRegular;font-weight:bold" class="cGray">ДИЗАЙН-ПРОЕКТА</h3>

						<h4 class="cGray size12" style="font-size: 18px; font-family: conMedium; margin: 0;">ПРИ ЗАКАЗЕ ПО ТАРИФУ ВСЁ ВКЛЮЧЕНО</h4>

						<p class="cGray" style="font-family: conRegular; font-size: 18px; margin: 5px 0px 10px;">до конца акции:</p>
						<div class="timer" style="float: right; margin: -4px -7px 0px 0px;">
							<div class="container">
								<div class="flip day1Play"></div>
								<div class="flip dayPlay">
									<em style="color: black;">дней</em>
								</div>
								<div class="flip hour2Play"></div>
								<div class="flip hourPlay">
									<em style="color: black;">часов</em>
								</div>
								<div class="flip minute6Play"></div>
								<div class="flip minutePlay">
									<em style="color: black;">минут</em>
								</div>
								<div class="flip second6Play"></div>
								<div class="flip secondPlay">
									<em style="color: black;">секунд</em>
								</div>
							</div>
						</div>
						<!--<div style="position: relative;" class="cGray">
					      <div id="countdown_dashboard2">
					        <div class="dash weeks_dash" style="display: none"> <span class="dash_title">недель</span>
					          <div class="digit">0</div>
					          <div class="digit">0</div>
					        </div>
					        <div class="dash days_dash"> <span class="dash_title">дней</span>
					          <div class="digit">0</div>
					          <div class="digit">0</div>
					        </div>
					        <div class="dash hours_dash"> <span class="dash_title">часов</span>
					          <div class="digit">0</div>
					          <div class="digit">0</div>
					        </div>
					        <div class="dash minutes_dash"> <span class="dash_title">минут</span>
					          <div class="digit">0</div>
					          <div class="digit">0</div>
					        </div>
					        <div class="dash seconds_dash"> <span class="dash_title">секунд</span>
					          <div class="digit">0</div>
					          <div class="digit">0</div>
					        </div>
					      </div>
					      <div class="clear"></div>
					    </div>	-->
					</section>
				</section>
				<section class="span4" style="margin:35px 0 0 20px;">
					<section class="formLeftCentr">
						<form method="post" action="" style="margin:0" name="Cons" onsubmit="return validateForm2()">
						<section>
							<h2 class="inlineBlockI cGray" style="margin: 7px -5px -8px 0px;"><strong style="font-size: 36px; font-family: conMedium; ">ОСТАВЬТЕ ЗАЯВКУ</strong></h2>
							<h2 class="inlineBlockI cGray"  style="font-size: 36px; margin: 5px 0px 14px 2px;"><strong>НА РАСЧЕТ</strong></h2>
						</section>
						<h3 class="designProjectText1">дизайн-проекта </h3>&nbsp&nbsp<h3 class="designProjectText1-1">со скидкой</h3>
						<section class="inputCenterStyle">
							<img src="img/userB.png" class="inlineTable" style="vertical-align:middle;">
							<input  name="Cons[name]" class=" inlineTable" type="text" style="margin:0px 0 0 0; font-size: 16px; font-family: conRegular;" placeholder="ваше имя*">
						</section>
						<section class="inputCenterStyle">
							<img src="img/phoneB.png" class="inlineTable" style="vertical-align:middle;">
							<input  name="Cons[phone]" class=" inlineTable" type="tel" style="margin:0px 0 0 0; font-size: 16px; font-family: conRegular;" onkeyup="formattingNumbers2( this )" placeholder="ваш телефон*">
						</section>
						<section class="inputCenterStyle">
							<img src="img/mailB.png" class="inlineTable" style="vertical-align:middle;">
							<input  class="inlineTable" type="email" name="Cons[email]" style="margin:0px 0 0 0; font-size: 16px; font-family: conRegular;" placeholder="ваш e-mail">
						</section>
						<input type="submit" id="sbmBtnCntr" class=" width229 submitBtnCentr" value="ОСТАВИТЬ ЗАЯВКУ">
						</form>
						<section class="textAlignCenter" style="margin:0px 0 0 0;  padding:0;">
							<p class="textRightGarantiya cGray">Мы гарантируем</p>
							<p class="textRightGarantiya cGray">конфидентиальность Ваших даных</p>
						</section>
					</section>
					<script>// onkeyup="formattingNumbers( this )" value="+ 7 "
					function validateForm2() {
					    var x = document.forms["Cons"]["Cons[phone]"].value.length;
					    if (x!=17) {
					        alert("Номер должен содержать 11 цифр!");
					        return false;
					    }
					    var y = document.forms["Engeneer"]["Engeneer[name]"].value.length;
					    var q = document.forms["Engeneer"]["Engeneer[email]"].value.length;
					    if (x!=17) {
					    	alert("Номер должен содержать 11 цифр!");
					        return false;
					    }

					    if (y=="") {
					        alert("Вы не заполнили имя!");
					        return false;
					    }
					    if (q=="") {
					        alert("Вы не заполнили email!");
					        return false;
					    }
					}
					function formattingNumbers2( elem ) 
					{
						var pattern = '+ 7 123 456-78-90', arr = elem.value.match( /\d/g ), i = 0;
						if ( arr === null ) return;
						elem.value = pattern.replace( /\d/g, function( a, b ) {
							if ( arr.length ) i = b + 1;
							return arr.shift();
						}).substring( 0, i );
					}
					</script>
				</section>
			</section>
		</section>
	</section>	
</section>
