
<section class="ring2Main">
	<section class="ring2Close" onclick="closering2()">X</section>
	<h2 style="color: #393939; margin: 9px 0px -11px 48px; padding: 30px 0 0 0;">Заказать</h2>
	<h2 style="color: #393939; margin: 10px 0 20px 0;">обратный звонок</h2>
	<h3 class="ring2secondText">Введите имя и телефон.<br>
Специалист компании GolHouse свяжется с Вами,<br>
чтобы ответить на Ваши вопросы.</h3>
	<form action="index.php" method="post" name="ring2" onsubmit="return validateFormring2()" style="margin: 0px 0px -5px;">
	<section class="ring2Inp ring2Cen">
		<section class="inInp">Введите Ваше имя</section>
		<input type="text" class="inlineTable" name="ring2[name]" onfocus="this.style.color='#000'; this.value='';" style="height: 50px; width: 393px; margin:0px 0 0 0;font-size: 24px; text-align: center; text-align: center; font-family: conRegular; color: #393939;" value="Иван Иванов">
	</section>
	<section class="ring2Inp ring2Cen">
		<section class="inInp">Введите Ваш телефон</section>
		<input type="text" class="inlineTable" name="ring2[phone]" style="margin:0px 0 0 0; font-size: 24px;;font-family: conRegular; height: 50px; text-align: center; width: 393px; color: #393939;" onkeyup="formattingNumbers2( this )" value="+7 (123) 456 78 90" onfocus="this.style.color='#000'; this.value='+7';">
	</section>
	<input type="submit" class="ring2But" value="Заказать звонок" style="font-family: conRegular; font-size: 32px; border-radius: 12px;  width: 405px; border: 2px solid #ffd400; padding: 15px 0px; margin: 20px 5px 5px 5px;">
	</form>
	<section>
		<p class="ring2Gar">или позвоните нам по телефону</p>
		<p class="ring2Gar">+7 (499) 408 12 12</p>
	</section>
</section>
<script>// onkeyup="formattingNumbers( this )" value="+ 7 "
	function myFunction()
	{
		document.forms["ring2"]["ring2[phone]"].value = "+7 ";
	}
	function validateFormring2() {
	    var x = document.forms["ring2"]["ring2[phone]"].value.length;
	    var y = document.forms["ring2"]["ring2[name]"].value.length;
	    if (x!=17) {
	    	alert("Номер должен содержать 11 цифр!");
	        return false;
	    }

	    if (y=="") {
	        alert("Вы не заполнили имя!");
	        return false;
	    }	    
	}
	function closering2()
	{
		$(".ring2Main").css("display","none");
		$(".allOfAll").css("display","none");
	}
	function openring2()
	{
		$(".ring2Main").css("display","block");
		$(".allOfAll").css("display","block");
	}
	function formattingNumbers2( elem ) 
	{
		var pattern = '+ 7 123 456-78-90', arr = elem.value.match( /\d/g ), i = 0;
		if ( arr === null ) return;
		elem.value = pattern.replace( /\d/g, function( a, b ) {
			if ( arr.length ) i = b + 1;
			return arr.shift();
		}).substring( 0, i );
	}
</script>