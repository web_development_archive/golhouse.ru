<section id="demand"></section>
<section class="bgAllTitles">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle" style="margin: 5px 9px 0px -9px;">ЧТО МЫ </h1>
		<h1 class="cBlue inlineTable textTitle">ПРЕДЛАГАЕМ</h1>
	</section>
</section>
<section class="allBg" style="height:920px;">
	<section class="container allContainerBg headerContBg">
		<section class="row bum">
			<section class="span4 textAlignCenter fLight" style="width: 270px; margin: 0px 0px 0px 18px;">
				<h2 class="cBlue titleDemand">БАЗОВЫЙ</h2>
				<img alt="" src="img/9demand/1.png">
				<h2 class="cBlue podTitleDemand">от 800 рублей/м2</h2>
				<ul>
					<li><p>Замер</p></li>
					<li><p>Проектирование будущей пла-<br>нировки (2 варианта)</p></li>
					<li><p>План расстановки мебели и <br>оборудования (3 варианта)</p></li>
					<li><p>Стилевое решение (1 вариант)</p></li>
					<li><p>3D-моделирование интерьера <br>одного помещения на выбор</p></li>
				</ul>
			</section>

			<section class="span4 textAlignCenter fLight;"  style="width: 270px; margin: 0 0 0 66px;">
				<h2 class="cBlue fLight titleDemand">ВСЁ ВКЛЮЧЕНО</h2>
				<img alt="" src="img/9demand/2.png">
				<h2 class="cBlue fLight podTitleDemand">от 1350 рублей/м2</h2>
				<ul style="margin: 0px 0px 0px 32px;">
					<li><p>Замер</p></li>
					<li><p>Проектирование будущей пла-<br>нировки <label>(3 варианта)</label></p></li>
					<li><p>План расстановки мебели и <br>оборудования (3 варианта)</p></li>
					<li><p>Стилевое решение <label>(2 варианта)</label></p></li>
					<li><p>3D-моделирование интерьера <br><label>до 4-х</label> картинок на выбор</p></li>
					<li><p><label>Альбом чертежей</label></p></li>
					<li><p><label>Авторский надзор 4 выезда в <br>течение месяца</label></p></li>
					<li><p><label>Консультация по подбору <br>мебели и материала</label></p></li>
					<li><p><label>1 выезд с дизайнером на <br>подбор материалов и мебели</label></p></li>
				</ul>
			</section>

			<section class="span4 textAlignCenter fLight;" style="width: 270px; margin: 0px -15px 0px 66px;">
				<h2 class="cBlue fLight titleDemand">ПРЕМИУМ</h2>
				<img alt="" src="img/9demand/3.png">
				<h2 class="cBlue podTitleDemand fLight">от 2700 рублей/м2</h2>
				<ul style="margin: 0px 0px 0px 42px;">
					<li><p>Замер</p></li>
					<li><p>Проектирование будущей пла-<br>нировки <label>(3 варианта)</label></p></li>
					<li><p>План расстановки мебели и <br>оборудования (3 варианта)</p></li>
					<li><p>Стилевое решение <label>(2 варианта)</label></p></li>
					<li><p>3D-моделирование интерьера<br><label>до 10-ти</label> картинок на выбор</p></li>
					<li><p><label>Альбом чертежей</label></p></li>
					<li><p><label>Авторский надзор на всё время <br>работ</label></p></li>
					<li><p><label>Консультации по подбору <br>мебели и материала</label></p></li>
					<li><p><label>Выезды с дизайнером на <br>подбор материалов и мебели</label></p></li>
					<li><p><label>Декорирование помещений</label></p></li>
					<li><p><label>Флористика</label></p></li>
				</ul>
				
			</section>
			<section class="btns"><br><br>
				<section class="btnVipV2" onclick="openZakaz1()">					
					ЗАКАЗАТЬ
				</section>
				<section class="btnVipV2" onclick="openZakaz2()">					
					ЗАКАЗАТЬ
				</section>
				<section class="btnVipV2" onclick="openZakaz3()">					
					ЗАКАЗАТЬ
				</section>
			</section>

		</section>
	</section>

</section>