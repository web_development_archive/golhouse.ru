<section id="preimushestva"></section>
<section class="bgAllTitles" >
	<section style="text-align:center;">
		<h1 class="inlineTable cGray textTitle">НАШИ</h1>
		<h1 class="cBlue inlineTable textTitle"  style="margin: 0px -6px 0px 6px;">ПРЕИМУЩЕСТВА</h1>
	</section>
</section>
<section class="benefitsBG allBg">
	<section class="container allContainerBg headerContBg">
		<section class="row">
			<section class="span6" style="margin:1px 0 0 50px;">
				<section class="benHeightNeed">
					<section class="inlineTable bumL">
						<section class="secL cBlack">
						<section class="cBlue headerL">
							БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ И ЗАМЕР
						</section>
						
							Наш замерщик <section class="pustBudetT">бесплатно</section> выезжает на объект для опреде-<br>ления площади и технических возможностей Вашего помеще-<br>ния. Дизайнер предоставляет первичную консультацию.
						</section>
					</section>
					<section class="inlineTable cBlue footerL">
						01
					</section>
				</section>
				<section class="benHeightNeed">
					<section class="inlineTable bumL">
						<section class="secL cBlack">
						<section class="cBlue headerL">
							РАБОТЫ ВЫПОЛНЯЮТСЯ ТОЧНО В СРОК
						</section>
							Каждый этап работ прописан в договоре и выполняет-<br>ся <section class="pustBudetT">без задержек</section>. Мы ценим свое и Ваше время.
						</section>
					</section>
					<section class="inlineTable cBlue footerL">
						03
					</section>
				</section>
				<section class="benHeightNeed">
					<section class="inlineTable bumL">
						<section style="color:black;" class="secL">
						<section class="cBlue headerL">
							ОПЫТ РАБОТЫ ОТ 8 ЛЕТ	
						</section>
							Наши специалисты имеют опыт создания проектов<br><section class="pustBudetT">более восьми лет</section>, как жилых, так и коммерческих<br> объектов.
						</section>
					</section>
					<section class="inlineTable cBlue footerL">
						05
					</section>
				</section>
				<section class="benHeightNeed" style="margin-top: -20px;">
					<section class="inlineTable bumL">
						<section class="secL cBlack">
						<section class="cBlue headerL">
							РАЦИОНАЛЬНЫЕ ПЛАНИРОВОЧНЫЕ РЕШЕНИЯ	
						</section>
							Наши планировочные решения максимально<br>удовлетворяют Вашим пожеланиям и техническим возможностям помещения.
						</section>
					</section>
					<section class="inlineTable cBlue footerL">
						07
					</section>
				</section>
			</section>
			<section class="span6" style="margin: 1px -47px 0 20px;">
				<section class="benHeightNeed">
					<section class="inlineTable cBlue footerR">
						02
					</section>
					<section class="inlineTable bumR">
						<section class="secR cBlack">
						<section class="cBlue headerR">
							С НАМИ ВЫ СЭКОНОМИТЕ
						</section>
							<section class="pustBudetT">Экономия средств</section> за счет покупки материалов. Мы<br>работаем давно и знаем лучшие места для приобрете-<br>ния материалов с лучшим соотношением цена/качество.
						</section>
					</section>
				</section>
				<section class="benHeightNeed">
					<section class="inlineTable cBlue footerR">
						04
					</section>
					<section class="inlineTable bumR">
						<section class="secR cBlack">
						<section class="cBlue headerR">
							ЗАЩИЩАЕМ ОТ ПОТЕРИ ВРЕМЕНИ
						</section>
							Максимально подробный проект позволяет <section class="pustBudetT">сразу при-<br>ступить</section> к просчету сметы. Ведомость отделочных<br>материалов, облегчает составления графиков прове-<br>дения работ и поставки материалов.
						</section>
					</section>
				</section>
				<section  class="benHeightNeed">
					<section class="inlineTable cBlue footerR">
						06
					</section>
					<section class="inlineTable bumR">
						<section class="secR cBlack">
						<section class="cBlue headerR">
							ЮРИДИЧЕСКАЯ ОТВЕТСТВЕННОСТЬ
						</section>
							Компания несет за свою работу юридическую ответ-<br>ственность. С каждым клиентом <section class="pustBudetT">заключается договор</section>,<br> в котором четко прописываются условия работы.
						</section>
					</section>
				</section>
				<section class="benHeightNeed" style="margin-top: -20px;">
					<section class="inlineTable cBlue footerR">
						08
					</section>
					<section class="inlineTable bumR">
						<section class="secR cBlack">
						<section class="cBlue headerR">
							КОМФОРТНАЯ РАБОТА
						</section>
							Широкий спектр задач успешно выполняется нашими<br>специалистами. Мы поможем сделать Ваш дом по-на-<br>стоящему уютным.
						</section>
					</section>
				</section>
			</section>
		</section>
	</section>

</section>