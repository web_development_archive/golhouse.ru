
<html class="cssmasks">

<head>
    <!--ololo-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" servergenerated="true">
             <!--
    Important Note About This Website's SEO

    Find the SEO content of this site's homepage via http://www.archi-dom.com/?_escaped_fragment_=
    (That is where search engines like Google go to read your homepage's content.)

    To view the SEO content of your internal pages, such as "заказ дизайна", go here: http://www.archi-dom.com/?_escaped_fragment_=-/cepm
    (That is where search engines like Google go to read the content on your internal pages.)

    For more information about Ajax Crawling technology, read Google's explanation here: https://developers.google.com/webmasters/ajax-crawling/
-->

    
                <meta charset="utf-8" servergenerated="true">
<title servergenerated="true">Архитектурное бюро ДОМ</title>
<meta name="fb_admins_meta_tag" content="" servergenerated="true">
<meta name="keywords" content="дизайн Дома Крым, дизайн дома, дизайн интерьера, дизайн интерьера Крым, дизайн интерьера Москва, дизайн интерьера Севастополь, дизайн интерьера Симферополь, дизайн проект интерьера" servergenerated="true">
<meta name="description" content="дизайн интерьера, ландшафтный дизайн, архитектурное проектирование" servergenerated="true">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Play:n,b,i,bi|&amp;subset=latin,cyrillic"><link rel="stylesheet" type="text/css" href="http://static.parastorage.com/services/web/2.1063.18/css/wysiwyg/user-site-fonts/cyrillic.css"><link rel="stylesheet" type="text/css" href="http://static.parastorage.com/services/web/2.1063.18/css/wysiwyg/user-site-fonts/latin.css"><link rel="shortcut icon" href="http://static.wix.com/client/pfavico.ico" type="image/png" servergenerated="true">
<link rel="apple-touch-icon" href="http://static.wix.com/client/pfavico.ico" type="image/png" servergenerated="true">
<link rel="canonical" href="http://www.archi-dom.com/" servergenerated="true">
    <meta http-equiv="X-Wix-Renderer-Server" content="apu2.aus" servergenerated="true">
<meta http-equiv="X-Wix-Meta-Site-Id" content="e054e2ca-ff1d-47db-ac3e-ce1c20ce750a" servergenerated="true">
<meta http-equiv="X-Wix-Application-Instance-Id" content="6fcd4a61-1216-4a73-bb84-77c9c3ebc223" servergenerated="true">
<meta http-equiv="X-Wix-Published-Version" content="404" servergenerated="true">
<meta http-equiv="etag" content="c2be0d41665fdd495a600f09714626c7" servergenerated="true">
<meta property="og:title" content="архитектурное бюро" servergenerated="true">
<meta property="og:type" content="website" servergenerated="true">
<meta property="og:url" content="http://www.archi-dom.com/" servergenerated="true">
<meta property="og:image" content="http://static.wixstatic.com/media/86cf10_04141a546f1b4bf38c14f1970a2b42c8.jpg" servergenerated="true">
<meta property="og:site_name" content="архитектурное бюро" servergenerated="true">
<meta property="og:description" content="дизайн интерьера, ландшафтный дизайн, архитектурное проектирование" servergenerated="true">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" servergenerated="true">

<meta id="wixMobileViewport" name="viewport" content="minimum-scale=0.25, maximum-scale=1.2" servergenerated="true">


        

    
    
<script id="BeatStatisticsAndKeepAliveJSONP" type="text/javascript" async="" src="//beat.wix.com/alive/jsonp?app=hve&amp;siteId=6fcd4a61-1216-4a73-bb84-77c9c3ebc223&amp;metaSiteId=e054e2ca-ff1d-47db-ac3e-ce1c20ce750a&amp;isTemplate=0&amp;pageFileName=архитектурное бюро&amp;firstTimeInDocPage=true&amp;etag=c2be0d41665fdd495a600f09714626c7"></script><script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-MDD5C4"></script><script async="">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MDD5C4');</script><script type="text/javascript" async="" src="http://stats.g.doubleclick.net/dc.js" servergenerated="true"></script><script type="text/javascript" servergenerated="true">(window.NREUM||(NREUM={})).loader_config={xpid:"VgUDU15ACQoGV1NUDg=="};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o?o:e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({QJf3ax:[function(t,e){function n(t){function e(e,n,a){t&&t(e,n,a),a||(a={});for(var c=s(e),f=c.length,u=i(a,o,r),d=0;f>d;d++)c[d].apply(u,n);return u}function a(t,e){f[t]=s(t).concat(e)}function s(t){return f[t]||[]}function c(){return n(e)}var f={};return{on:a,emit:e,create:c,listeners:s,_events:f}}function r(){return{}}var o="nr@context",i=t("gos");e.exports=n()},{gos:"7eSDFh"}],ee:[function(t,e){e.exports=t("QJf3ax")},{}],3:[function(t){function e(t,e,n,i,s){try{c?c-=1:r("err",[s||new UncaughtException(t,e,n)])}catch(f){try{r("ierr",[f,(new Date).getTime(),!0])}catch(u){}}return"function"==typeof a?a.apply(this,o(arguments)):!1}function UncaughtException(t,e,n){this.message=t||"Uncaught error with no additional information",this.sourceURL=e,this.line=n}function n(t){r("err",[t,(new Date).getTime()])}var r=t("handle"),o=t(5),i=t("ee"),a=window.onerror,s=!1,c=0;t("loader").features.err=!0,window.onerror=e,NREUM.noticeError=n;try{throw new Error}catch(f){"stack"in f&&(t(1),t(4),"addEventListener"in window&&t(2),window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&t(3),s=!0)}i.on("fn-start",function(){s&&(c+=1)}),i.on("fn-err",function(t,e,r){s&&(this.thrown=!0,n(r))}),i.on("fn-end",function(){s&&!this.thrown&&c>0&&(c-=1)}),i.on("internal-error",function(t){r("ierr",[t,(new Date).getTime(),!0])})},{1:8,2:5,3:9,4:7,5:20,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],4:[function(t){function e(){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var n=t("ee"),r=t("handle"),o=t(2);t("loader").features.stn=!0,t(1),n.on("fn-start",function(t){var e=t[0];e instanceof Event&&(this.bstStart=Date.now())}),n.on("fn-end",function(t,e){var n=t[0];n instanceof Event&&r("bst",[n,e,this.bstStart,Date.now()])}),o.on("fn-start",function(t,e,n){this.bstStart=Date.now(),this.bstType=n}),o.on("fn-end",function(t,e){r("bstTimer",[e,this.bstStart,Date.now(),this.bstType])}),n.on("pushState-start",function(){this.time=Date.now(),this.startPath=location.pathname+location.hash}),n.on("pushState-end",function(){r("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),"addEventListener"in window.performance&&(window.performance.addEventListener("webkitresourcetimingbufferfull",function(){r("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.webkitClearResourceTimings()},!1),window.performance.addEventListener("resourcetimingbufferfull",function(){r("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.clearResourceTimings()},!1)),document.addEventListener("scroll",e,!1),document.addEventListener("keypress",e,!1),document.addEventListener("click",e,!1)}},{1:6,2:8,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],5:[function(t,e){function n(t){i.inPlace(t,["addEventListener","removeEventListener"],"-",r)}function r(t){return t[1]}var o=(t(1),t("ee").create()),i=t(2)(o),a=t("gos");if(e.exports=o,n(window),"getPrototypeOf"in Object){for(var s=document;s&&!s.hasOwnProperty("addEventListener");)s=Object.getPrototypeOf(s);s&&n(s);for(var c=XMLHttpRequest.prototype;c&&!c.hasOwnProperty("addEventListener");)c=Object.getPrototypeOf(c);c&&n(c)}else XMLHttpRequest.prototype.hasOwnProperty("addEventListener")&&n(XMLHttpRequest.prototype);o.on("addEventListener-start",function(t){if(t[1]){var e=t[1];"function"==typeof e?this.wrapped=t[1]=a(e,"nr@wrapped",function(){return i(e,"fn-",null,e.name||"anonymous")}):"function"==typeof e.handleEvent&&i.inPlace(e,["handleEvent"],"fn-")}}),o.on("removeEventListener-start",function(t){var e=this.wrapped;e&&(t[1]=e)})},{1:20,2:21,ee:"QJf3ax",gos:"7eSDFh"}],6:[function(t,e){var n=(t(2),t("ee").create()),r=t(1)(n);e.exports=n,r.inPlace(window.history,["pushState"],"-")},{1:21,2:20,ee:"QJf3ax"}],7:[function(t,e){var n=(t(2),t("ee").create()),r=t(1)(n);e.exports=n,r.inPlace(window,["requestAnimationFrame","mozRequestAnimationFrame","webkitRequestAnimationFrame","msRequestAnimationFrame"],"raf-"),n.on("raf-start",function(t){t[0]=r(t[0],"fn-")})},{1:21,2:20,ee:"QJf3ax"}],8:[function(t,e){function n(t,e,n){var r=t[0];"string"==typeof r&&(r=new Function(r)),t[0]=o(r,"fn-",null,n)}var r=(t(2),t("ee").create()),o=t(1)(r);e.exports=r,o.inPlace(window,["setTimeout","setInterval","setImmediate"],"setTimer-"),r.on("setTimer-start",n)},{1:21,2:20,ee:"QJf3ax"}],9:[function(t,e){function n(){c.inPlace(this,d,"fn-")}function r(t,e){c.inPlace(e,["onreadystatechange"],"fn-")}function o(t,e){return e}var i=t("ee").create(),a=t(1),s=t(2),c=s(i),f=s(a),u=window.XMLHttpRequest,d=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"];e.exports=i,window.XMLHttpRequest=function(t){var e=new u(t);try{i.emit("new-xhr",[],e),f.inPlace(e,["addEventListener","removeEventListener"],"-",function(t,e){return e}),e.addEventListener("readystatechange",n,!1)}catch(r){try{i.emit("internal-error",[r])}catch(o){}}return e},window.XMLHttpRequest.prototype=u.prototype,c.inPlace(XMLHttpRequest.prototype,["open","send"],"-xhr-",o),i.on("send-xhr-start",r),i.on("open-xhr-start",r)},{1:5,2:21,ee:"QJf3ax"}],10:[function(t){function e(t){if("string"==typeof t&&t.length)return t.length;if("object"!=typeof t)return void 0;if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if("undefined"!=typeof FormData&&t instanceof FormData)return void 0;try{return JSON.stringify(t).length}catch(e){return void 0}}function n(t){var n=this.params,r=this.metrics;if(!this.ended){this.ended=!0;for(var i=0;c>i;i++)t.removeEventListener(s[i],this.listener,!1);if(!n.aborted){if(r.duration=(new Date).getTime()-this.startTime,4===t.readyState){n.status=t.status;var a=t.responseType,f="arraybuffer"===a||"blob"===a||"json"===a?t.response:t.responseText,u=e(f);if(u&&(r.rxSize=u),this.sameOrigin){var d=t.getResponseHeader("X-NewRelic-App-Data");d&&(n.cat=d.split(", ").pop())}}else n.status=0;r.cbTime=this.cbTime,o("xhr",[n,r,this.startTime])}}}function r(t,e){var n=i(e),r=t.params;r.host=n.hostname+":"+n.port,r.pathname=n.pathname,t.sameOrigin=n.sameOrigin}if(window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent)){t("loader").features.xhr=!0;var o=t("handle"),i=t(2),a=t("ee"),s=["load","error","abort","timeout"],c=s.length,f=t(1);t(4),t(3),a.on("new-xhr",function(){this.totalCbs=0,this.called=0,this.cbTime=0,this.end=n,this.ended=!1,this.xhrGuids={}}),a.on("open-xhr-start",function(t){this.params={method:t[0]},r(this,t[1]),this.metrics={}}),a.on("open-xhr-end",function(t,e){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&e.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),a.on("send-xhr-start",function(t,n){var r=this.metrics,o=t[0],i=this;if(r&&o){var f=e(o);f&&(r.txSize=f)}this.startTime=(new Date).getTime(),this.listener=function(t){try{"abort"===t.type&&(i.params.aborted=!0),("load"!==t.type||i.called===i.totalCbs&&(i.onloadCalled||"function"!=typeof n.onload))&&i.end(n)}catch(e){try{a.emit("internal-error",[e])}catch(r){}}};for(var u=0;c>u;u++)n.addEventListener(s[u],this.listener,!1)}),a.on("xhr-cb-time",function(t,e,n){this.cbTime+=t,e?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof n.onload||this.end(n)}),a.on("xhr-load-added",function(t,e){var n=""+f(t)+!!e;this.xhrGuids&&!this.xhrGuids[n]&&(this.xhrGuids[n]=!0,this.totalCbs+=1)}),a.on("xhr-load-removed",function(t,e){var n=""+f(t)+!!e;this.xhrGuids&&this.xhrGuids[n]&&(delete this.xhrGuids[n],this.totalCbs-=1)}),a.on("addEventListener-end",function(t,e){e instanceof XMLHttpRequest&&"load"===t[0]&&a.emit("xhr-load-added",[t[1],t[2]],e)}),a.on("removeEventListener-end",function(t,e){e instanceof XMLHttpRequest&&"load"===t[0]&&a.emit("xhr-load-removed",[t[1],t[2]],e)}),a.on("fn-start",function(t,e,n){e instanceof XMLHttpRequest&&("onload"===n&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=(new Date).getTime()))}),a.on("fn-end",function(t,e){this.xhrCbStart&&a.emit("xhr-cb-time",[(new Date).getTime()-this.xhrCbStart,this.onload,e],e)})}},{1:"XL7HBI",2:11,3:9,4:5,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],11:[function(t,e){e.exports=function(t){var e=document.createElement("a"),n=window.location,r={};e.href=t,r.port=e.port;var o=e.href.split("://");return!r.port&&o[1]&&(r.port=o[1].split("/")[0].split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=e.hostname||n.hostname,r.pathname=e.pathname,"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname),r.sameOrigin=!e.hostname||e.hostname===document.domain&&e.port===n.port&&e.protocol===n.protocol,r}},{}],gos:[function(t,e){e.exports=t("7eSDFh")},{}],"7eSDFh":[function(t,e){function n(t,e,n){if(r.call(t,e))return t[e];var o=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return t[e]=o,o}var r=Object.prototype.hasOwnProperty;e.exports=n},{}],D5DuLP:[function(t,e){function n(t,e,n){return r.listeners(t).length?r.emit(t,e,n):(o[t]||(o[t]=[]),void o[t].push(e))}var r=t("ee").create(),o={};e.exports=n,n.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(t,e){e.exports=t("D5DuLP")},{}],XL7HBI:[function(t,e){function n(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:i(t,o,function(){return r++})}var r=1,o="nr@id",i=t("gos");e.exports=n},{gos:"7eSDFh"}],id:[function(t,e){e.exports=t("XL7HBI")},{}],loader:[function(t,e){e.exports=t("G9z0Bl")},{}],G9z0Bl:[function(t,e){function n(){var t=p.info=NREUM.info;if(t&&t.agent&&t.licenseKey&&t.applicationID&&c&&c.body){p.proto="https"===d.split(":")[0]||t.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var e=c.createElement("script");e.src=p.proto+t.agent,c.body.appendChild(e)}}function r(){"complete"===c.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=t("handle"),s=window,c=s.document,f="addEventListener",u="attachEvent",d=(""+location).split("?")[0],p=e.exports={offset:i(),origin:d,features:{}};c[f]?(c[f]("DOMContentLoaded",o,!1),s[f]("load",n,!1)):(c[u]("onreadystatechange",r),s[u]("onload",n)),a("mark",["firstbyte",i()])},{handle:"D5DuLP"}],20:[function(t,e){function n(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(0>o?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=n},{}],21:[function(t,e){function n(t){return!(t&&"function"==typeof t&&t.apply&&!t[i])}var r=t("ee"),o=t(1),i="nr@wrapper",a=Object.prototype.hasOwnProperty;e.exports=function(t){function e(t,e,r,a){function nrWrapper(){var n,i,s,f;try{i=this,n=o(arguments),s=r&&r(n,i)||{}}catch(d){u([d,"",[n,i,a],s])}c(e+"start",[n,i,a],s);try{return f=t.apply(i,n)}catch(p){throw c(e+"err",[n,i,p],s),p}finally{c(e+"end",[n,i,f],s)}}return n(t)?t:(e||(e=""),nrWrapper[i]=!0,f(t,nrWrapper),nrWrapper)}function s(t,r,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<r.length;c++)s=r[c],a=t[s],n(a)||(t[s]=e(a,f?s+o:o,i,s,t))}function c(e,n,r){try{t.emit(e,n,r)}catch(o){u([o,e,n,r])}}function f(t,e){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(t);return n.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(r){u([r])}for(var o in t)a.call(t,o)&&(e[o]=t[o]);return e}function u(e){try{t.emit("internal-error",e)}catch(n){}}return t||(t=r),e.inPlace=s,e.flag=i,e}},{1:20,ee:"QJf3ax"}]},{},["G9z0Bl",3,10,4]);</script><style type="text/css" servergenerated="true"></style>

    <!-- META DATA -->
<script type="text/javascript" servergenerated="true">
    var rendererModel = {"debugMode":"nodebug","previewMode":false,"serviceMappings":{"2":{"idInMetaSite":2,"idInApp":"6fcd4a61-1216-4a73-bb84-77c9c3ebc223","applicationType":"HtmlWeb"}},"metaSiteId":"e054e2ca-ff1d-47db-ac3e-ce1c20ce750a","premiumFeatures":["AdsFree","ShowWixWhileLoading","HasDomain"],"siteId":"6fcd4a61-1216-4a73-bb84-77c9c3ebc223","userId":"86cf1099-2e85-4342-b5e7-8e5a01b65472","published":true,"revision":404,"applicationType":"HtmlWeb","documentType":"UGC","siteTitleSEO":"архитектурное бюро","clientSpecMap":{"14":{"type":"wixapps","applicationId":14,"appDefinitionId":"61f33d50-3002-4882-ae86-d319c1a249ab","datastoreId":"138a97d0-454b-4176-08c4-e5dc902158a2","packageName":"blog","state":"Initialized","widgets":{"56ab6fa4-95ac-4391-9337-6702b8a77011":{"widgetId":"56ab6fa4-95ac-4391-9337-6702b8a77011","defaultHeight":400,"defaultWidth":210},"31c0cede-09db-4ec7-b760-d375d62101e6":{"widgetId":"31c0cede-09db-4ec7-b760-d375d62101e6","defaultHeight":600,"defaultWidth":680},"1b8c501f-ccc2-47e7-952a-47e264752614":{"widgetId":"1b8c501f-ccc2-47e7-952a-47e264752614","defaultHeight":280,"defaultWidth":916},"33a9f5e0-b083-4ccc-b55d-3ca5d241a6eb":{"widgetId":"33a9f5e0-b083-4ccc-b55d-3ca5d241a6eb","defaultHeight":220,"defaultWidth":210},"c7f57b50-8940-4ff1-83c6-6756d6f0a1f4":{"widgetId":"c7f57b50-8940-4ff1-83c6-6756d6f0a1f4","defaultHeight":220,"defaultWidth":210},"f72fe377-8abc-40f2-8656-89cfe00f3a22":{"widgetId":"f72fe377-8abc-40f2-8656-89cfe00f3a22","defaultHeight":300,"defaultWidth":210},"c340212a-6e2e-45cd-9dc4-58d01a5b63a7":{"widgetId":"c340212a-6e2e-45cd-9dc4-58d01a5b63a7","defaultHeight":300,"defaultWidth":210},"ea63bc0f-c09f-470c-ac9e-2a408b499f22":{"widgetId":"ea63bc0f-c09f-470c-ac9e-2a408b499f22","defaultHeight":800,"defaultWidth":800},"4de5abc5-6da2-4f97-acc3-94bb74285072":{"widgetId":"4de5abc5-6da2-4f97-acc3-94bb74285072","defaultHeight":800,"defaultWidth":800},"e000b4bf-9ff1-4e66-a0d3-d4b365ba3af5":{"widgetId":"e000b4bf-9ff1-4e66-a0d3-d4b365ba3af5","defaultHeight":400,"defaultWidth":210},"43c2a0a8-f224-4a29-bd19-508114831a3a":{"widgetId":"43c2a0a8-f224-4a29-bd19-508114831a3a","defaultHeight":40,"defaultWidth":210}}},"1":{"type":"wixapps","applicationId":1,"appDefinitionId":"e4c4a4fb-673d-493a-9ef1-661fa3823ad7","datastoreId":"138a97d0-412a-dbb5-c145-18b9d31dad24","packageName":"menu","state":"Initialized","widgets":{"1660c5f3-b183-4e6c-a873-5d6bbd918224":{"widgetId":"1660c5f3-b183-4e6c-a873-5d6bbd918224","defaultHeight":100,"defaultWidth":400}}},"13":{"type":"sitemembers","applicationId":13,"collectionType":"Open","smcollectionId":"3da71f40-c629-427e-9d7a-6cd1632a595e"},"2":{"type":"appbuilder","applicationId":2,"appDefinitionId":"3d590cbc-4907-4cc4-b0b1-ddf2c5edf297","instanceId":"138a97d0-44b4-7cce-e5f9-16d74904ca5d","state":"Initialized"},"17":{"type":"public","applicationId":17,"appDefinitionId":"1311ad42-7453-f88b-2576-3704c9c08c51","appDefinitionName":"Active Contact Form","instance":"wfpd3MpxuwIYTcvhnlR6S7oRctN56CNEwO67Twv6kd8.eyJpbnN0YW5jZUlkIjoiMTM5MWVhOTQtZmJiNC1jMjdlLWNiM2ItZDYzYzA4ZDJjMzBkIiwic2lnbkRhdGUiOiIyMDE0LTEwLTMwVDE3OjUwOjU5Ljk4OFoiLCJpcEFuZFBvcnQiOiI4Ny4yNDcuNTQuMjQ4LzQ1NDI1IiwiZGVtb01vZGUiOmZhbHNlfQ","sectionPublished":true,"sectionMobilePublished":false,"sectionSeoEnabled":true,"widgets":{"1311ad56-ce95-66e4-e74a-68e8b45e1f89":{"widgetUrl":"http:\/\/www.vcita.com\/integrations\/wix\/widget?app_type=active_engage","widgetId":"1311ad56-ce95-66e4-e74a-68e8b45e1f89","refreshOnWidthChange":true,"gluedOptions":{"placement":"BOTTOM_RIGHT","verticalMargin":0.0,"horizontalMargin":0.0},"published":true,"mobilePublished":false,"seoEnabled":true}},"appRequirements":{"requireSiteMembers":false},"installedAtDashboard":true,"permissions":{"revoked":false}},"16":{"type":"public","applicationId":16,"appDefinitionId":"135c3d92-0fea-1f9d-2ba5-2a1dfb04297e","appDefinitionName":"Send Beautiful Newsletters","instance":"SuCqEtIgfeijYXPBWLYJXmFSnLC31w6bI8it5dNGk6s.eyJpbnN0YW5jZUlkIjoiMTM4YzU5MTAtOTI5Yy1iNzdhLTYxNDQtOGQ3NDdiOWMxMDllIiwic2lnbkRhdGUiOiIyMDE0LTEwLTMwVDE3OjUwOjU5Ljk4OFoiLCJpcEFuZFBvcnQiOiI4Ny4yNDcuNTQuMjQ4LzQ1NDI1IiwiZGVtb01vZGUiOmZhbHNlfQ","sectionPublished":true,"sectionMobilePublished":false,"sectionSeoEnabled":true,"widgets":{},"appRequirements":{"requireSiteMembers":false},"installedAtDashboard":true,"permissions":{"revoked":false}},"15":{"type":"ecommerce","applicationId":15,"appDefinitionId":"55a88716-958a-4b91-b666-6c1118abdee4","magentoStoreId":"45979811","packageName":"ecommerce","widgets":{"30b4a102-7649-47d9-a60b-bfd89dcca135":{"widgetId":"30b4a102-7649-47d9-a60b-bfd89dcca135","defaultHeight":585,"defaultWidth":960},"adbeffec-c7df-4908-acd0-cdd23155a817":{"widgetId":"adbeffec-c7df-4908-acd0-cdd23155a817","defaultHeight":150,"defaultWidth":500},"f72a3898-8520-4b60-8cd6-24e4e20d483d":{"widgetId":"f72a3898-8520-4b60-8cd6-24e4e20d483d","defaultHeight":600,"defaultWidth":840},"c029b3fd-e8e4-44f1-b1f0-1f83e437d45c":{"widgetId":"c029b3fd-e8e4-44f1-b1f0-1f83e437d45c","defaultHeight":50,"defaultWidth":200},"cd54a28f-e3c9-4522-91c4-15e6dd5bc514":{"widgetId":"cd54a28f-e3c9-4522-91c4-15e6dd5bc514","defaultHeight":50,"defaultWidth":200},"c614fb79-dbec-4ac7-b9b0-419669fadecc":{"widgetId":"c614fb79-dbec-4ac7-b9b0-419669fadecc","defaultHeight":50,"defaultWidth":200},"5fca0e8b-a33c-4c18-b8eb-da50d7f31e4a":{"widgetId":"5fca0e8b-a33c-4c18-b8eb-da50d7f31e4a","defaultHeight":150,"defaultWidth":800},"ae674d74-b30b-47c3-aba0-0bd220e25a69":{"widgetId":"ae674d74-b30b-47c3-aba0-0bd220e25a69","defaultHeight":150,"defaultWidth":220},"fbd55289-7136-4c7d-955c-3088974c1f93":{"widgetId":"fbd55289-7136-4c7d-955c-3088974c1f93","defaultHeight":150,"defaultWidth":220}},"state":"Initialized"}},"runningExperiments":{"mobileactionsmenu":"new","atntfixlists":"new","appbuilderdeletetype":"new","sitenavigationrefactor":"new","animationnewbehaviors":"new","backgroundpresetter":"new","lesserwidthissue":"new","redirectfeature301":"new","wixappsgalleries":"new","sendbionpagejsontrialfail":"new","blogrss":"new","animation3dfix":"new","ecomgalleries":"new","disablehorizontalmenu":"new","redirectfeature301data":"new","editorloadperpage":"new","sitepagesvalidation":"new","linkfixeroverride":"new","nougcanalytics":"new","appbuildertags":"new","fixpagescontainergap":"new","customsitemenu":"new","backgroundperpage":"new","ngcore":"new","formsfailurefallback":"new","subscribeformsendnewsletter":"new","exitmobilemode":"new","workaroundsaveddeadcompskin":"new","lazyprovision":"new","landingpagesupport":"new","wixappstranslation":"new"},"languageCode":"ru","scriptsCacheKiller":1,"siteMetaData":{"preloader":{"enabled":true},"hasMobileStructure":false,"quickActions":{"socialLinks":[],"colorScheme":"dark","configuration":{"quickActionsMenuEnabled":true,"navigationMenuEnabled":true,"phoneEnabled":true,"emailEnabled":true,"addressEnabled":false,"socialLinksEnabled":false}},"contactInfo":{"companyName":"","phone":"+79787431589","fax":"","email":"info@archi-dom.com","address":"Республика Крым, г. Симферополь"}},"geo":"KAZ"};
    var publicModel = {"externalBaseUrl":"http://www.archi-dom.com/","domain":"archi-dom.com","premiumFeatures":["AdsFree","ShowWixWhileLoading","HasDomain"],"language":"ru","favicon":"","trackingData":{"googleAnalyticsId":"UA-55232986-1"},"suppressTrackingCookies":false,"pageList":{"masterPage":["http://static.parastorage.com/sites/86cf10_eaa625b0ede4dca2ce7d65d300cd087c_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_eaa625b0ede4dca2ce7d65d300cd087c_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_eaa625b0ede4dca2ce7d65d300cd087c_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_eaa625b0ede4dca2ce7d65d300cd087c_402.json"],"pages":[{"pageId":"mainPage","title":"УСЛУГИ","urls":["http://static.parastorage.com/sites/86cf10_789c4f83b29e43b5c5cce4210cfb815f_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_789c4f83b29e43b5c5cce4210cfb815f_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_789c4f83b29e43b5c5cce4210cfb815f_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_789c4f83b29e43b5c5cce4210cfb815f_402.json"]},{"pageId":"cepm","title":"заказ дизайна","urls":["http://static.parastorage.com/sites/86cf10_02c42ed7fc6ebbf88ec980d8047d31ed_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_02c42ed7fc6ebbf88ec980d8047d31ed_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_02c42ed7fc6ebbf88ec980d8047d31ed_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_02c42ed7fc6ebbf88ec980d8047d31ed_402.json"]},{"pageId":"c1ivp","title":"предметный дизайн","urls":["http://static.parastorage.com/sites/86cf10_612c28db97b87cca4e3d69fbc8cad1bc_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_612c28db97b87cca4e3d69fbc8cad1bc_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_612c28db97b87cca4e3d69fbc8cad1bc_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_612c28db97b87cca4e3d69fbc8cad1bc_402.json"]},{"pageId":"c1048","title":"вопрос менеджеру","urls":["http://static.parastorage.com/sites/86cf10_b5448d338372c512bcd856faff848052_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_b5448d338372c512bcd856faff848052_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_b5448d338372c512bcd856faff848052_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_b5448d338372c512bcd856faff848052_402.json"]},{"pageId":"c50i","title":"дизайн интерьера","urls":["http://static.parastorage.com/sites/86cf10_8c90d12d9a1a4dd180ac97eff05be0b4_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_8c90d12d9a1a4dd180ac97eff05be0b4_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_8c90d12d9a1a4dd180ac97eff05be0b4_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_8c90d12d9a1a4dd180ac97eff05be0b4_402.json"]},{"pageId":"c1a7d","title":"обратный звонок","urls":["http://static.parastorage.com/sites/86cf10_37be1ff9e29dbdf19d262bc89f73122c_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_37be1ff9e29dbdf19d262bc89f73122c_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_37be1ff9e29dbdf19d262bc89f73122c_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_37be1ff9e29dbdf19d262bc89f73122c_402.json"]},{"pageId":"cl78","title":"оставить заявку","urls":["http://static.parastorage.com/sites/86cf10_5a8c3552944675cc004bd24c2394d210_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_5a8c3552944675cc004bd24c2394d210_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_5a8c3552944675cc004bd24c2394d210_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_5a8c3552944675cc004bd24c2394d210_402.json"]},{"pageId":"c1h7r","title":"заказ архитектуры","urls":["http://static.parastorage.com/sites/86cf10_a4d07e3c974188ce38f88172522e07db_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_a4d07e3c974188ce38f88172522e07db_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_a4d07e3c974188ce38f88172522e07db_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_a4d07e3c974188ce38f88172522e07db_402.json"]},{"pageId":"cym0","title":"архитектурное проектирование","urls":["http://static.parastorage.com/sites/86cf10_3a907ebae54904f862977b4a031f7b61_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_3a907ebae54904f862977b4a031f7b61_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_3a907ebae54904f862977b4a031f7b61_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_3a907ebae54904f862977b4a031f7b61_402.json"]},{"pageId":"c1shc","title":"авторский надзор","urls":["http://static.parastorage.com/sites/86cf10_e10f1a8afe8be878e79f97529f0c17bb_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_e10f1a8afe8be878e79f97529f0c17bb_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_e10f1a8afe8be878e79f97529f0c17bb_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_e10f1a8afe8be878e79f97529f0c17bb_402.json"]},{"pageId":"c626","title":"ландшафтная архитектура","urls":["http://static.parastorage.com/sites/86cf10_caea9902eaeada798fa5aa2e0ae8088e_402.json.z?v=2","http://static.wixstatic.com/sites/86cf10_caea9902eaeada798fa5aa2e0ae8088e_402.json.z?v=2","http://www.archi-dom.com/sites/86cf10_caea9902eaeada798fa5aa2e0ae8088e_402.json.z?v=2","http://fallback.wix.com/wix-html-editor-pages-webapp/page/86cf10_caea9902eaeada798fa5aa2e0ae8088e_402.json"]}],"mainPageId":"mainPage"},"siteRevision":404,"timeSincePublish":640319191,"adaptiveMobileOn":false};
    var serviceTopology = {"serverName":"apu2.aus","cacheKillerVersion":"1","staticServerUrl":"http://static.parastorage.com/","usersScriptsRoot":"http://static.parastorage.com/services/wix-users/2.446.0","biServerUrl":"http://frog.wix.com/","userServerUrl":"http://users.wix.com/","billingServerUrl":"http://premium.wix.com/","mediaRootUrl":"http://static.wixstatic.com/","logServerUrl":"http://frog.wix.com/plebs","monitoringServerUrl":"http://TODO/","usersClientApiUrl":"https://users.wix.com/wix-users","publicStaticBaseUri":"http://static.parastorage.com/services/wix-public/1.111.0","basePublicUrl":"http://www.wix.com/","postLoginUrl":"http://www.wix.com/my-account","postSignUpUrl":"http://www.wix.com/new/account","baseDomain":"wix.com","staticMediaUrl":"http://static.wixstatic.com/media","staticAudioUrl":"http://storage.googleapis.com/static.wixstatic.com/mp3","emailServer":"http://assets.wix.com/common-services/notification/invoke","blobUrl":"http://static.parastorage.com/wix_blob","htmlEditorUrl":"http://editor.wix.com/html","siteMembersUrl":"https://users.wix.com/wix-sm","scriptsLocationMap":{"bootstrap":"http://static.parastorage.com/services/bootstrap/2.1063.18","it":"http://static.parastorage.com/services/experiments/it/1.37.0","santa-versions":"http://static.parastorage.com/services/santa-versions/1.41.0","automation":"http://static.parastorage.com/services/automation/1.23.0","ecommerce":"http://static.parastorage.com/services/ecommerce/1.184.11","wixapps":"http://static.parastorage.com/services/wixapps/2.432.0","web":"http://static.parastorage.com/services/web/2.1063.18","ut":"http://static.parastorage.com/services/experiments/ut/1.2.0","tpa":"http://static.parastorage.com/services/tpa/2.925.0","ck-editor":"http://static.parastorage.com/services/ck-editor/1.87.0","sitemembers":"http://static.parastorage.com/services/sm-js-sdk/1.31.0","hotfixes":"http://static.parastorage.com/services/experiments/hotfixes/1.12.0","langs":"http://static.parastorage.com/services/langs/2.467.0","core":"http://static.parastorage.com/services/core/2.1063.18","skins":"http://static.parastorage.com/services/skins/2.1063.18"},"developerMode":false,"productionMode":true,"userFilesUrl":"http://static.parastorage.com/","staticHTMLComponentUrl":"http://www.archi-dom.com.usrfiles.com/","secured":false,"ecommerceCheckoutUrl":"https://www.safer-checkout.com/","premiumServerUrl":"https://premium.wix.com/","appRepoUrl":"http://assets.wix.com/wix-lists-ds-webapp","digitalGoodsServerUrl":"http://dgs.wixapps.net/","wixCloudBaseUrl":"http://cloud.wix.com/","publicStaticsUrl":"http://static.parastorage.com/services/wix-public/1.111.0","staticDocsUrl":"http://media.wix.com/ugd"};
    var siteHeader = {"id":"6fcd4a61-1216-4a73-bb84-77c9c3ebc223", "userId":"86cf1099-2e85-4342-b5e7-8e5a01b65472"};
    var siteId = siteHeader.id;

    var configUrls = serviceTopology;

    var debugMode = "nodebug";
    var viewMode = (rendererModel.previewMode) ? 'preview' : 'site';

    var googleAnalytics = "UA-55232986-1"
    ;

</script>


    <meta name="fragment" content="!" servergenerated="true">
    <!-- DATA -->
<script type="text/javascript" servergenerated="true">
    var adData = {};
    var mobileAdData = {};
    var usersDomain = "https://users.wix.com//wix-users";
        </script>



            <script src="http://static.parastorage.com/services/wix-users/2.446.0/client/js/userApi_v2.js?cacheKiller=1" servergenerated="true"></script>
            <script src="http://static.parastorage.com/services/wix-users/2.446.0/user-api/user-api.min.js?cacheKiller=1" servergenerated="true"></script>
    
    
            <script type="text/javascript" servergenerated="true">
            var userApi = UserApi.getInstance().init({
                "usersDomain":"http://users.wix.com//wix-users",
                "corsEnabled":false,
                "dontHandShake":true,
                "urlThatUserRedirectedFrom":"$"
            });

            UserServerApi.setOptions({
                "usersDomain":"http://users.wix.com//wix-users",
                "urlParams": { "urlThatUserRedirectedFrom" : "$" }
            });
        </script>
                <style type="text/css" servergenerated="true">#lleo_dialog, #lleo_dialog * {
    margin: 0 !important;
    padding: 0 !important;
    background: none !important;
    border: none 0 !important;
    position: static !important;
    vertical-align: baseline !important;
    font: normal 13px Arial, Helvetica !important;
    line-height: 15px !important;
    color: #000 !important;
    overflow: visible !important;
    width: auto !important;
    height: auto !important;
    float: none !important;
    visibility: visible !important;
    text-align: left !important;
    border-collapse: separate !important;
    border-spacing: 2px !important;
}

#lleo_dialog iframe {
    height: 0 !important;
    width: 0 !important;
}

#lleo_dialog {
    position: absolute !important;
    background: #fff !important;
    border: solid 1px #ccc !important;
    padding: 7px 0 0 !important;
    left: -999px;
    top: -999px;
    /*max-width: 450px !important;*/
    width: 440px !important;
    overflow: hidden;
    display: block !important;
    z-index: 999999999 !important;
    opacity: 0 !important;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.18) !important;
    -moz-border-radius: 3px !important;
    -webkit-border-radius: 3px !important;
    border-radius: 3px !important;
}
#lleo_dialog.lleo_show {
    opacity: 1 !important;
    -webkit-transition: opacity 0.3s !important;
}
#lleo_dialog input::-webkit-input-placeholder {
    color: #aaa !important;
}
#lleo_dialog .lleo_has_pic #lleo_word {
    margin-right: 80px !important;
}
#lleo_dialog #lleo_translationsCopntainer1 {
    position: relative !important;
}
#lleo_dialog #lleo_translationsCopntainer2 {
    padding: 7px 0 0 !important;
    vertical-align: middle !important;
}
#lleo_dialog #lleo_word {
    color: #000 !important;
    margin: 0 5px 2px 0 !important;
    /*float: left !important;*/
}
#lleo_dialog .lleo_has_sound #lleo_word {
    margin-left: 17px !important;
}
#lleo_dialog #lleo_text {
    font-weight: bold !important;
    color: #d56e00 !important;
    text-decoration: none !important;
    cursor: default !important;
}
#lleo_dialog #lleo_text.lleo_known {
    cursor: pointer !important;
    text-decoration: underline !important;
}
#lleo_dialog #lleo_closeBtn {
    position: absolute !important;
    right: 6px !important;
    top: 5px !important;
    line-height: 1px !important;
    text-decoration: none !important;
    font-weight: bold !important;
    font-size: 0 !important;
    color: #aaa !important;
    display: block !important;
    padding: 2px !important;
    z-index: 9999999999 !important;
    width: 7px !important;
    height: 7px !important;
    padding: 0  !important;
    margin: 0   !important;
}

#lleo_dialog #lleo_optionsBtn {
    position: absolute !important; 
    right: 1px !important;
    top: 12px !important;
    line-height: 1px !important;
    text-decoration: none !important;
    font-weight: bold !important;
    font-size: 13px !important;
    color: #aaa !important;
    padding: 2px !important;
    display: none;
}
#lleo_dialog #lleo_optionsBtn img{
    width: 12px !important;
    height: 12px !important;
}
#lleo_dialog #lleo_sound {
    float: left !important;
    width: 16px !important;
    height: 16px !important;
    margin-left: 12px !important;
    background: 0 0 no-repeat !important;
    cursor: pointer !important;
    display: none !important;
}
#lleo_dialog .lleo_has_sound #lleo_sound {
    display: block !important;
}
#lleo_dialog #lleo_picOuter {
    position: absolute !important;
    float: right !important;
    right: 29px;
    top: 0;
    display: none !important;
    z-index: 9 !important;
}
#lleo_dialog .lleo_has_pic #lleo_picOuter {
    display: block !important;
}
#lleo_dialog #lleo_picOuter:hover {
    z-index: 11 !important;
}
#lleo_dialog #lleo_pic,
#lleo_dialog #lleo_picBig {
    position: absolute !important;
    top: 0 !important;
    right: 0 !important;
    border: solid 2px #fff !important;
    -moz-border-radius: 2px !important;
    -webkit-border-radius: 2px !important;
    border-radius: 2px !important;
    z-index: 1 !important;
}
#lleo_dialog #lleo_pic {
    position: relative !important;
    border: none !important;
    width: 34px !important;
}
#lleo_dialog #lleo_picBig {
    box-shadow: -1px 2px 4px rgba(0,0,0,0.3);
    z-index: 2 !important;
    opacity: 0 !important;
    visibility: hidden !important;
}
#lleo_dialog #lleo_picOuter:hover #lleo_picBig {
    visibility: visible !important;
    opacity: 1 !important;
    -webkit-transition: opacity 0.3s !important;
    -webkit-transition-delay: 0.3s !important;
}
#lleo_dialog #lleo_transcription {
    color: #486D85 !important;
    margin: 0 0 4px 29px !important;
    color: #aaaaaa !important;
}
#lleo_dialog .lleo_no_trans {
    color: #aaa !important;
}






#lleo_dialog .ll-translation-counter {
    float: right !important;
    font-size: 11px !important;
    color: #aaa !important;
    padding: 2px 2px 1px 10px !important;
}

#lleo_dialog .ll-translation-text {
    float: left !important;
    width: 80% !important;
}

#lleo_dialog #lleo_trans a {
    color: #3F669F !important;
    padding: 1px 4px !important;
    text-decoration: none !important;
    text-overflow: ellipsis !important;
    overflow: hidden !important;
}

#lleo_dialog .ll-translation-item {
    width: 100% !important;
    float: left !important; 
    padding:1px 4px;
    color: #3F669F !important;
    padding: 3px !important;
    border: solid 1px white !important;
    -moz-border-radius: 2px !important;
    -webkit-border-radius: 2px !important;
    border-radius: 2px !important;
}

#lleo_dialog .ll-translation-item:hover {
    border: solid 1px #9FC2C9 !important;
    background: #EDF4F6 !important;
    cursor: pointer !important;
}

#lleo_dialog .ll-translation-marker {
    margin: 0px 5px 2px 2px !important;
}





#lleo_dialog #lleo_icons {
    margin: 10px 0 7px !important;
    color: #aaa !important;
    line-height: 20px !important;
    font-size: 11px !important;
    clear: both !important;
    padding-left: 16px !important;
}
#lleo_icons a {
    display: inline-block !important;
    width: 16px !important;
    height: 16px !important;
    margin: 0 0 -2px 3px !important;
    text-decoration: none !important;
    background: 0 0 no-repeat !important;
    opacity: 0.5 !important;
}
#lleo_icons a:hover {
    opacity: 1 !important;
}
#lleo_icons a.lleo_google     {background-position:-34px 0 !important;}
#lleo_icons a.lleo_multitran  {background-position:-64px 0 !important;}
#lleo_icons a.lleo_lingvo     {background-position:-51px 0 !important; width: 12px !important;}
#lleo_icons a.lleo_dict       {background-position:-17px 0 !important;}
#lleo_icons a.lleo_linguee    {background-position:-81px 0 !important;}
#lleo_icons a.lleo_michaelis  {background-position:-98px 0 !important;}




#lleo_dialog #lleo_contextContainer {
    margin: 0 !important;
    padding: 3px 15px 3px 10px !important;
    background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee)) !important;
    border-bottom: solid 1px #ddd !important;
    border-top-left-radius: 3px !important;
    border-top-right-radius: 3px !important;
    display: none !important;
    overflow: hidden !important;
}
#lleo_dialog .lleo_has_context #lleo_contextContainer {
    display: block !important;
}
#lleo_dialog #lleo_context {
    color: #444 !important;
    text-shadow: 1px 1px 0 #f4f4f4 !important;
    line-height: 12px !important;
    font-size: 11px !important;
    margin-left: 2px !important;
}
#lleo_dialog #lleo_context b {
    line-height: 12px !important;
    color: #000 !important;
    font-weight: bold !important;
    font-size: 11px !important;
}
#lleo_dialog #lleo_gBrand {
    color: #aaa !important;
    font-size: 10px !important;
    /*padding-right: 52px !important;*/
    padding-bottom: 14px !important;
    margin: -3px 4px 0 4px !important;
    background: left bottom no-repeat !important;
    display: inline-block !important;
    float: right !important;
}
#lleo_dialog #lleo_gBrand.hidden {
    display: none !important;
}
#lleo_dialog #lleo_translateContextLink {
    color: #444 !important;
    text-shadow: 1px 1px 0 #f4f4f4 !important;
    background: -webkit-gradient(linear, left top, left bottom, from(#f4f4f4), to(#ddd)) !important;
    border: solid 1px !important;
    box-shadow: 1px 1px 0 #f6f6f6 !important;
    border-color: #999 #aaa #aaa #999 !important;
    -moz-border-radius: 2px !important;
    -webkit-border-radius: 2px !important;
    border-radius: 2px !important;
    padding: 0 3px !important;
    font-size: 11px !important;
    text-decoration: none !important;
    margin: 1px 5px 0 !important;
    display: inline-block !important;
    white-space: nowrap !important;
}
#lleo_dialog #lleo_translateContextLink:hover {
    background: #f8f8f8 !important;
}

#lleo_dialog #lleo_setTransForm {
    display: block !important;
    margin-top: 3px !important;
    padding-top: 5px !important;
    /* Set position and background because the form might be overlapped by an image when no translations */
    position: relative !important;
    background: #fff !important;
    z-index: 10 !important;
    padding-bottom: 10px !important;
    padding-left: 16px !important;
}
#lleo_dialog .lleo-custom-translation {
    padding: 4px 5px !important;
    border: solid 1px #ddd !important;
    -moz-border-radius: 2px !important;
    -webkit-border-radius: 2px !important;
    border-radius: 2px !important;
    width: 90% !important;
    min-width: 270px !important;
    background: -webkit-gradient(linear, 0 0, 0 20, from(#f1f1f1), to(#fff)) !important;
    font: normal 13px Arial, Helvetica !important;
    line-height: 15px !important;
}
#lleo_dialog .lleo-custom-translation:hover {
    border: solid 1px #aaa !important;
}
#lleo_dialog .lleo-custom-translation:focus {
    background: #FFFEC9 !important;
}

#lleo_dialog *.hidden {
    display: none !important;
}

#lleo_dialog .infinitive{
    color: #D56E00 !important;
    text-decoration: none;
    border-bottom: 1px dotted #D56E00 !important;
}
#lleo_dialog .infinitive:hover{
    border: none !important;
}


#lleo_dialog #lleo_trans{
    zoom: 1;
    border-top: 1px solid #eeeeee !important;
    margin: 10px 0 0 !important;
    padding: 5px 30px 0 14px !important;
}

#lleo_dialog .lleo_clearfix {
    display: block !important;
    clear: both !important;
    visibility: hidden !important;
    height: 0 !important;
    font-size: 0 !important;
}

#lleo_dialog #lleo_markBlock {
    background: #eeeeee !important;
    cursor: pointer !important; 
    border-bottom-left-radius: 3px !important;
    border-bottom-right-radius: 3px !important;
    border-collapse: separate !important;
    border-spacing: 2px !important; 
}

#lleo_dialog #lleo_markBlock img{
    width: 14px !important;
    height: 14px !important;
}

#lleo_dialog #lleo_markBlock .icon-cell {
    padding: 5px 2px 5px 16px !important;
    height: 17px !important;
}

#lleo_dialog #lleo_markBlock .wide-cell {
    width: 100% !important;
}

#lleo_dialog #lleo_markBlock .text-cell {
    color: #999999 !important; 
    font: normal 13px Arial, Helvetica !important;
    text-shadow: 0 1px #fff !important; 
}

#lleo_dialog #lleo_markBlock td {
    vertical-align: middle !important;
    border-collapse: separate !important;
    border-spacing: 2px !important;
}


#lleo_dialog #lleo_picOuter table{
    width: 44px !important;
    position: absolute !important;
    right: 0 !important;
    vertical-align: middle !important;
}

#lleo_dialog #lleo_picOuter td{
    width: 38px !important;
    height: 38px !important;
    border: 1px solid #eeeeee !important;
    vertical-align: middle !important;
    text-align: center !important;
}

#lleo_dialog #lleo_picOuter td div {
    height: 38px !important;
    overflow: hidden !important;
}</style><style type="text/css" servergenerated="true">
.ll-content-notification *{
    letter-spacing: normal !important;
    margin: 0 !important;
    padding: 0 !important;
    background: none !important;
    border: 0 !important;
    float: none !important;
    text-align: left !important;
    text-decoration: none !important;
    font: normal 15px 'Lucida Grande', 'Lucida Sans Unicode', Lucida, Arial, Helvetica, sans-serif !important;
}


.ll-content-notification {
    vertical-align: baseline !important;
    color: #000 !important;
    overflow: visible !important;
    visibility: visible !important;
    margin: 0 !important;
    padding: 0 !important;
    position: fixed !important;
    background: #fff !important;
    border: solid 1px #AAA !important;
    /*
    left: -999px;
    top: -999px;
    */
    width: auto;
    /* width: 300px !important; */
    display: block;
    z-index: 999999999 !important;
    -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, 0.18) !important;
    -moz-box-shadow: 0 2px 4px rgba(0, 0, 0, 0.18) !important;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.18) !important;
    -webkit-border-radius: 3px !important;
    -moz-border-radius: 3px !important;
    border-radius: 3px !important;
    overflow: hidden !important;
    /* opacity: 0 !important; */
    transition: opacity 0.8s !important;
    -moz-transition: opacity 0.8s !important; /* Firefox 4 */
    -webkit-transition: opacity 0.8s !important; /* Safari and Chrome */
    -o-transition: opacity 0.8s !important; /* Opera */
    cursor: default !important;
}

.ll-content-notification-shown {
    opacity: 1 !important;
    transition: opacity 0.8s !important;
    -moz-transition: opacity 0.8s !important; /* Firefox 4 */
    -webkit-transition: opacity 0.8s !important; /* Safari and Chrome */
    -o-transition: opacity 0.8s !important; /* Opera */
}

.ll-content-notification-header {
    border: 0 !important;
    margin: 0 !important;
    background: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAIAAABsYngUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAABJJREFUGFdjePHmCxw9e/UZjgAVYhYtk8xZqAAAAABJRU5ErkJggg==) !important;
    border-bottom: solid 1px #CCC !important;
    padding: 1px 4px !important;
    min-height: 18px !important;
    width: 100% !important;
    -webkit-border-top-left-radius: 3px !important;
    -webkit-border-top-right-radius: 3px !important;
    -moz-border-radius-topleft: 3px !important;
    -moz-border-radius-topright: 3px !important;
    border-top-left-radius: 3px !important;
    border-top-right-radius: 3px !important;
    border-collapse: collapse !important;
    border-spacing: 0 !important;
}

.ll-content-notification-header-pic {
    border: 0 !important;
    margin: 0 !important;
    padding: 3px 0 0 3px !important;
    width: 20px !important;
    vertical-align: top !important;
    line-height: 1px !important;
}

.ll-content-notification-header-pic img{
    border: 0 !important;
    padding: 0 !important;
    margin: 0 !important;
    line-height: 1px !important;
}


.ll-content-notification-header-caption {
    font: normal 13px 'Lucida Grande', 'Lucida Sans Unicode', Lucida, Arial, Helvetica, sans-serif !important;
    font-weight: bold !important;
    line-height: 15px !important;
    color: #555 !important;
    float: left !important;
    text-shadow: none !important;
    letter-spacing: normal !important;
    white-space: normal !important;
    padding: 3px !important;
    margin: 0 !important;
}

.ll-content-notification-header-close {
    width: 15px !important;
    vertical-align: top !important;
    text-align: right !important;
    padding: 6px 5px 0 0 !important;
    margin: 0 !important;
    line-height: 1px !important;
}

.ll-content-notification-header-close img {
    border: 0 !important;
    width: 7px !important;
    height: 7px !important;
    margin: 0 !important;
    padding: 0 !important;
}

.ll-content-notification-content {
    margin: 0 !important;
    padding: 8px !important;
    float: left !important;
    overflow: hidden !important;
    width: auto !important;
}

.ll-content-notification-content-logo {
    float: left !important;
    height: 48px !important;
    width: 48px !important;
}

.ll-content-notification-content-main {
    margin-left: 60px !important;
    overflow: hidden !important;
    padding: 0 0 2px 0 !important;
    color: #333 !important;
    text-align: left !important;
    text-shadow: none !important;
    letter-spacing: normal !important;
    font: normal 13px 'Lucida Grande', 'Lucida Sans Unicode', Lucida, Arial, Helvetica, sans-serif !important;
    line-height: 15px !important;
    width: auto !important;
}

.ll-content-notification-content-header {
    text-align: left !important;
    text-decoration: none !important;
    font: bold 15px 'Lucida Grande', 'Lucida Sans Unicode', Lucida, Arial, Helvetica, sans-serif !important;
    line-height: 19px !important;
    margin: 0 0 4px 0 !important;
    padding: 0 !important;
    border: 0 !important;
    color: #333 !important;
    text-shadow: none !important;
    letter-spacing: normal !important;
    display: block !important;
    top: 0 !important;
    left: 0 !important;
}

.ll-content-notification-word {
    color: #d56e00 !important;
    font-weight: bold !important;
    font-size: 14px !important;
}
</style><link type="text/css" href="http://static.parastorage.com/services/web/2.1063.18/css/wysiwyg/viewer.min.css" rel="stylesheet" servergenerated="true"><style id="testCss"></style><style id="WIX_THEME_STYLES_i1wertl3"></style><style id="WIX_SKIN_STYLES_i1wertm4"></style><script type="text/javascript" async="" src="http://fast.fonts.net/t/trackingCode.js"></script><link type="text/css" rel="stylesheet" href="http://fast.fonts.net/t/1.css?apiType=css&amp;projectid=undefined">
<!--ololo-->
<meta charset="utf-8">
    
<title>
</title>
    
    
<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js">
</script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
</script>
    
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js">
</script>
    
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.2.1/lodash.min.js">
</script>
    
<script type="text/javascript" src="//sslstatic.wix.com/services/js-sdk/1.21.0/js/Wix.js">
</script>

    

<link rel="stylesheet" type="text/css" href="../css/style.min.css">
    
<script type="text/javascript" src="../js/app.min.js">
</script>

<style type="text/css">#lleo_dialog, #lleo_dialog * {
    margin: 0 !important;
    padding: 0 !important;
    background: none !important;
    border: none 0 !important;
    position: static !important;
    vertical-align: baseline !important;
    font: normal 13px Arial, Helvetica !important;
    line-height: 15px !important;
    color: #000 !important;
    overflow: visible !important;
    width: auto !important;
    height: auto !important;
    float: none !important;
    visibility: visible !important;
    text-align: left !important;
    border-collapse: separate !important;
    border-spacing: 2px !important;
    }

    #lleo_dialog iframe {
        height: 0 !important;
        width: 0 !important;
    }

    #lleo_dialog {
        position: absolute !important;
        background: #fff !important;
        border: solid 1px #ccc !important;
        padding: 7px 0 0 !important;
        left: -999px;
        top: -999px;
        /*max-width: 450px !important;*/
        width: 440px !important;
        overflow: hidden;
        display: block !important;
        z-index: 999999999 !important;
        opacity: 0 !important;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.18) !important;
        -moz-border-radius: 3px !important;
        -webkit-border-radius: 3px !important;
        border-radius: 3px !important;
    }
    #lleo_dialog.lleo_show {
        opacity: 1 !important;
        -webkit-transition: opacity 0.3s !important;
    }
    #lleo_dialog input::-webkit-input-placeholder {
        color: #aaa !important;
    }
    #lleo_dialog .lleo_has_pic #lleo_word {
        margin-right: 80px !important;
    }
    #lleo_dialog #lleo_translationsCopntainer1 {
        position: relative !important;
    }
    #lleo_dialog #lleo_translationsCopntainer2 {
        padding: 7px 0 0 !important;
        vertical-align: middle !important;
    }
    #lleo_dialog #lleo_word {
        color: #000 !important;
        margin: 0 5px 2px 0 !important;
        /*float: left !important;*/
    }
    #lleo_dialog .lleo_has_sound #lleo_word {
        margin-left: 17px !important;
    }
    #lleo_dialog #lleo_text {
        font-weight: bold !important;
        color: #d56e00 !important;
        text-decoration: none !important;
        cursor: default !important;
    }
    #lleo_dialog #lleo_text.lleo_known {
        cursor: pointer !important;
        text-decoration: underline !important;
    }
    #lleo_dialog #lleo_closeBtn {
        position: absolute !important;
        right: 6px !important;
        top: 5px !important;
        line-height: 1px !important;
        text-decoration: none !important;
        font-weight: bold !important;
        font-size: 0 !important;
        color: #aaa !important;
        display: block !important;
        padding: 2px !important;
        z-index: 9999999999 !important;
        width: 7px !important;
        height: 7px !important;
        padding: 0  !important;
        margin: 0   !important;
    }

    #lleo_dialog #lleo_optionsBtn {
        position: absolute !important; 
        right: 1px !important;
        top: 12px !important;
        line-height: 1px !important;
        text-decoration: none !important;
        font-weight: bold !important;
        font-size: 13px !important;
        color: #aaa !important;
        padding: 2px !important;
        display: none;
    }
    #lleo_dialog #lleo_optionsBtn img{
        width: 12px !important;
        height: 12px !important;
    }
    #lleo_dialog #lleo_sound {
        float: left !important;
        width: 16px !important;
        height: 16px !important;
        margin-left: 12px !important;
        background: 0 0 no-repeat !important;
        cursor: pointer !important;
        display: none !important;
    }
    #lleo_dialog .lleo_has_sound #lleo_sound {
        display: block !important;
    }
    #lleo_dialog #lleo_picOuter {
        position: absolute !important;
        float: right !important;
        right: 29px;
        top: 0;
        display: none !important;
        z-index: 9 !important;
    }
    #lleo_dialog .lleo_has_pic #lleo_picOuter {
        display: block !important;
    }
    #lleo_dialog #lleo_picOuter:hover {
        z-index: 11 !important;
    }
    #lleo_dialog #lleo_pic,
    #lleo_dialog #lleo_picBig {
        position: absolute !important;
        top: 0 !important;
        right: 0 !important;
        border: solid 2px #fff !important;
        -moz-border-radius: 2px !important;
        -webkit-border-radius: 2px !important;
        border-radius: 2px !important;
        z-index: 1 !important;
    }
    #lleo_dialog #lleo_pic {
        position: relative !important;
        border: none !important;
        width: 34px !important;
    }
    #lleo_dialog #lleo_picBig {
        box-shadow: -1px 2px 4px rgba(0,0,0,0.3);
        z-index: 2 !important;
        opacity: 0 !important;
        visibility: hidden !important;
    }
    #lleo_dialog #lleo_picOuter:hover #lleo_picBig {
        visibility: visible !important;
        opacity: 1 !important;
        -webkit-transition: opacity 0.3s !important;
        -webkit-transition-delay: 0.3s !important;
    }
    #lleo_dialog #lleo_transcription {
        color: #486D85 !important;
        margin: 0 0 4px 29px !important;
        color: #aaaaaa !important;
    }
    #lleo_dialog .lleo_no_trans {
        color: #aaa !important;
    }






    #lleo_dialog .ll-translation-counter {
        float: right !important;
        font-size: 11px !important;
        color: #aaa !important;
        padding: 2px 2px 1px 10px !important;
    }

    #lleo_dialog .ll-translation-text {
        float: left !important;
        width: 80% !important;
    }

    #lleo_dialog #lleo_trans a {
        color: #3F669F !important;
        padding: 1px 4px !important;
        text-decoration: none !important;
        text-overflow: ellipsis !important;
        overflow: hidden !important;
    }

    #lleo_dialog .ll-translation-item {
        width: 100% !important;
        float: left !important; 
        padding:1px 4px;
        color: #3F669F !important;
        padding: 3px !important;
        border: solid 1px white !important;
        -moz-border-radius: 2px !important;
        -webkit-border-radius: 2px !important;
        border-radius: 2px !important;
    }

    #lleo_dialog .ll-translation-item:hover {
        border: solid 1px #9FC2C9 !important;
        background: #EDF4F6 !important;
        cursor: pointer !important;
    }

    #lleo_dialog .ll-translation-marker {
        margin: 0px 5px 2px 2px !important;
    }





    #lleo_dialog #lleo_icons {
        margin: 10px 0 7px !important;
        color: #aaa !important;
        line-height: 20px !important;
        font-size: 11px !important;
        clear: both !important;
        padding-left: 16px !important;
    }
    #lleo_icons a {
        display: inline-block !important;
        width: 16px !important;
        height: 16px !important;
        margin: 0 0 -2px 3px !important;
        text-decoration: none !important;
        background: 0 0 no-repeat !important;
        opacity: 0.5 !important;
    }
    #lleo_icons a:hover {
        opacity: 1 !important;
    }
    #lleo_icons a.lleo_google     {background-position:-34px 0 !important;}
    #lleo_icons a.lleo_multitran  {background-position:-64px 0 !important;}
    #lleo_icons a.lleo_lingvo     {background-position:-51px 0 !important; width: 12px !important;}
    #lleo_icons a.lleo_dict       {background-position:-17px 0 !important;}
    #lleo_icons a.lleo_linguee    {background-position:-81px 0 !important;}
    #lleo_icons a.lleo_michaelis  {background-position:-98px 0 !important;}




    #lleo_dialog #lleo_contextContainer {
        margin: 0 !important;
        padding: 3px 15px 3px 10px !important;
        background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee)) !important;
        border-bottom: solid 1px #ddd !important;
        border-top-left-radius: 3px !important;
        border-top-right-radius: 3px !important;
        display: none !important;
        overflow: hidden !important;
    }
    #lleo_dialog .lleo_has_context #lleo_contextContainer {
        display: block !important;
    }
    #lleo_dialog #lleo_context {
        color: #444 !important;
        text-shadow: 1px 1px 0 #f4f4f4 !important;
        line-height: 12px !important;
        font-size: 11px !important;
        margin-left: 2px !important;
    }
    #lleo_dialog #lleo_context b {
        line-height: 12px !important;
        color: #000 !important;
        font-weight: bold !important;
        font-size: 11px !important;
    }
    #lleo_dialog #lleo_gBrand {
        color: #aaa !important;
        font-size: 10px !important;
        /*padding-right: 52px !important;*/
        padding-bottom: 14px !important;
        margin: -3px 4px 0 4px !important;
        background: left bottom no-repeat !important;
        display: inline-block !important;
        float: right !important;
    }
    #lleo_dialog #lleo_gBrand.hidden {
        display: none !important;
    }
    #lleo_dialog #lleo_translateContextLink {
        color: #444 !important;
        text-shadow: 1px 1px 0 #f4f4f4 !important;
        background: -webkit-gradient(linear, left top, left bottom, from(#f4f4f4), to(#ddd)) !important;
        border: solid 1px !important;
        box-shadow: 1px 1px 0 #f6f6f6 !important;
        border-color: #999 #aaa #aaa #999 !important;
        -moz-border-radius: 2px !important;
        -webkit-border-radius: 2px !important;
        border-radius: 2px !important;
        padding: 0 3px !important;
        font-size: 11px !important;
        text-decoration: none !important;
        margin: 1px 5px 0 !important;
        display: inline-block !important;
        white-space: nowrap !important;
    }
    #lleo_dialog #lleo_translateContextLink:hover {
        background: #f8f8f8 !important;
    }

    #lleo_dialog #lleo_setTransForm {
        display: block !important;
        margin-top: 3px !important;
        padding-top: 5px !important;
        /* Set position and background because the form might be overlapped by an image when no translations */
        position: relative !important;
        background: #fff !important;
        z-index: 10 !important;
        padding-bottom: 10px !important;
        padding-left: 16px !important;
    }
    #lleo_dialog .lleo-custom-translation {
        padding: 4px 5px !important;
        border: solid 1px #ddd !important;
        -moz-border-radius: 2px !important;
        -webkit-border-radius: 2px !important;
        border-radius: 2px !important;
        width: 90% !important;
        min-width: 270px !important;
        background: -webkit-gradient(linear, 0 0, 0 20, from(#f1f1f1), to(#fff)) !important;
        font: normal 13px Arial, Helvetica !important;
        line-height: 15px !important;
    }
    #lleo_dialog .lleo-custom-translation:hover {
        border: solid 1px #aaa !important;
    }
    #lleo_dialog .lleo-custom-translation:focus {
        background: #FFFEC9 !important;
    }

    #lleo_dialog *.hidden {
        display: none !important;
    }

    #lleo_dialog .infinitive{
        color: #D56E00 !important;
        text-decoration: none;
        border-bottom: 1px dotted #D56E00 !important;
    }
    #lleo_dialog .infinitive:hover{
        border: none !important;
    }


    #lleo_dialog #lleo_trans{
        zoom: 1;
        border-top: 1px solid #eeeeee !important;
        margin: 10px 0 0 !important;
        padding: 5px 30px 0 14px !important;
    }

    #lleo_dialog .lleo_clearfix {
        display: block !important;
        clear: both !important;
        visibility: hidden !important;
        height: 0 !important;
        font-size: 0 !important;
    }

    #lleo_dialog #lleo_markBlock {
        background: #eeeeee !important;
        cursor: pointer !important; 
        border-bottom-left-radius: 3px !important;
        border-bottom-right-radius: 3px !important;
        border-collapse: separate !important;
        border-spacing: 2px !important; 
    }

    #lleo_dialog #lleo_markBlock img{
        width: 14px !important;
        height: 14px !important;
    }

    #lleo_dialog #lleo_markBlock .icon-cell {
        padding: 5px 2px 5px 16px !important;
        height: 17px !important;
    }

    #lleo_dialog #lleo_markBlock .wide-cell {
        width: 100% !important;
    }

    #lleo_dialog #lleo_markBlock .text-cell {
        color: #999999 !important; 
        font: normal 13px Arial, Helvetica !important;
        text-shadow: 0 1px #fff !important; 
    }

    #lleo_dialog #lleo_markBlock td {
        vertical-align: middle !important;
        border-collapse: separate !important;
        border-spacing: 2px !important;
    }


    #lleo_dialog #lleo_picOuter table{
        width: 44px !important;
        position: absolute !important;
        right: 0 !important;
        vertical-align: middle !important;
    }

    #lleo_dialog #lleo_picOuter td{
        width: 38px !important;
        height: 38px !important;
        border: 1px solid #eeeeee !important;
        vertical-align: middle !important;
        text-align: center !important;
    }

    #lleo_dialog #lleo_picOuter td div {
        height: 38px !important;
        overflow: hidden !important;
    }
</style>
</head>


<body>


<div id="wrapper">
    

<div id="viewport" class="grid" data-mode="site" style="overflow: hidden; width: 1266px; height: 500px;">

<div id="display" class="active" style="width: 1266px; height: 442px; margin-bottom: 6px; overflow: hidden;">

<div class="prev" style="background-color: white;">

<div class="inner" style="background-color: black;">
</div>
</div>

<div class="next" style="background-color: white;">

<div class="inner" style="background-color: black;">
</div>
</div>

<div id="displayCycle" data-cycle-slides=">div" data-cycle-timeout="2000" data-cycle-auto-height="false" data-cycle-paused="true" data-cycle-fx="carousel" data-cycle-log="false" data-cycle-speed="1000" data-cycle-pager="#thumbnails" data-starting-slide="0" data-cycle-pager-template="

<div class='thumb' style='display:inline-block; width:{{thumbWidth}}px; height:{{thumbHeight}}px; background: url({{thumb}}) 50% no-repeat'>

<div class='overlay'>
</div>
</div>" data-cycle-prev="#display .prev" data-cycle-next="#display .next" data-allow-wrap="true" data-cycle-carousel-offset="453" data-cycle-easing="easeOutQuart" style="position: relative; overflow: hidden; width: 1266px; height: 442px;">

<div class="cycle-carousel-wrap" style="margin: 0px; padding: 0px; top: 0px; left: -7888px; position: absolute; white-space: nowrap;">

<div data-thumb="http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 353px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;">

<div class="filler" style="width: 353px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_353_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 353px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 315px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;">

<div class="filler" style="width: 315px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_315_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 315px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 442px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;">

<div class="filler" style="width: 442px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_442_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 442px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 663px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;">

<div class="filler" style="width: 663px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_663_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 663px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 353px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;">

<div class="filler" style="width: 353px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_353_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 353px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 315px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;">

<div class="filler" style="width: 315px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_315_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 315px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 442px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;">

<div class="filler" style="width: 442px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_442_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 442px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 663px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;">

<div class="filler" style="width: 663px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_663_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 663px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 353px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;">

<div class="filler" style="width: 353px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_353_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 353px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 315px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;">

<div class="filler" style="width: 315px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_315_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 315px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide cycle-slide-active" style="width: 442px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;">

<div class="filler" style="width: 442px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_442_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 442px; height: 442px; cursor: pointer; opacity: 0; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 663px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;">

<div class="filler" style="width: 663px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_663_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 663px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 353px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;">

<div class="filler" style="width: 353px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_353_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 353px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 315px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;">

<div class="filler" style="width: 315px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_315_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 315px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 442px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;">

<div class="filler" style="width: 442px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_442_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 442px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 663px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;">

<div class="filler" style="width: 663px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_663_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 663px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 353px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;">

<div class="filler" style="width: 353px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_353_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 353px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 662px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;">

<div class="filler" style="width: 662px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_662_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 662px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 315px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;">

<div class="filler" style="width: 315px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_315_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 315px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 442px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;">

<div class="filler" style="width: 442px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_442_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 442px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>

<div data-thumb="http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 663px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;">

<div class="filler" style="width: 663px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_663_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;">
</div>

<div class="overlay" style="width: 663px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;">
</div>
</div>
</div>
</div>
</div>

<div class="ui-widget-content hoverscroll horizontal" style="width: 978px; height: 52px;">

<div class="listcontainer" style="width: 978px; height: 52px;">

<div id="thumbnails" class="list" style="width: 555px; height: 48px; padding: 0px 1px;">

<div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_8cea3d22a5834a16b588e53874a0080d.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;">

<div class="overlay" style="opacity: 0.5;">
</div>
</div>

<div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_4e2a8cfe64c9418f9fa2b4ffde051dad.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;">

<div class="overlay" style="opacity: 0.5;">
</div>
</div>

<div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_70015ee163944310a5bc10efe11845f8.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;">

<div class="overlay" style="opacity: 0.5;">
</div>
</div>

<div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_bbed76975c5c4136991b6f74a636ad97.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;">

<div class="overlay" style="opacity: 0.5;">
</div>
</div>

<div class="thumb item cycle-pager-active" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_d291d9de794e402cb019d32e649ab146.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;">

<div class="overlay current" style="opacity: 0;">
</div>
</div>

<div class="thumb item" style="display:inline-block; width:88px; height:50px; background: url(http://static.wix.com/media/86cf10_230e515ff71e4704856d0faffdc2a39b.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% no-repeat">

<div class="overlay" style="opacity: 0.5;">
</div>
</div>
</div>
</div>
</div>
</div>

</div>

<script type="text/javascript">
    (function(SCOPE){
        var viewport = $('#viewport');
        var App = new StripShowcaseController(viewport, {});
    }(window));

</script>



        <script src="http://static.parastorage.com/services/web/2.1063.18/libs/hammer.min.js" servergenerated="true"></script>
        <script src="http://static.parastorage.com/services/web/2.1063.18/javascript/wysiwyg/viewer/quick_actions_new_menus.js" servergenerated="true"></script>
        <script servergenerated="true">
            if (MobileUtils.isMobile() && MobileUtils.isViewportOpen()){
                WQuickActions.element.style.visibility = "visible";
                WQuickActions.initialize();
            } else if(WQuickActions && WQuickActions.element){
                WQuickActions.element.style.display = 'none';
            }
        </script>
    
 
            <!-- debug mode=nodebug -->
<!-- anc2 -->
<script type="text/javascript" servergenerated="true">
var anchors = {};


</script>

    
    
        
    
<!-- No Footer -->
    
    
            <script servergenerated="true">
            window.define && define.resource('status.structure.loaded', true);
        </script>
    
    
    
<script type="text/javascript" servergenerated="true">window.NREUM||(NREUM={});NREUM.info={"applicationID":"1963269,1963270","applicationTime":29,"beacon":"beacon-6.newrelic.com","queueTime":0,"licenseKey":"c99d7f1ab0","transactionName":"ZFAHNkNYXUBQVEUKXF0aKDRyFmRWU39FDl9hUAsGVEtWQR9FVA1XVkc=","agent":"js-agent.newrelic.com\/nr-476.min.js","errorBeacon":"bam.nr-data.net"}</script>

</body>
</html>