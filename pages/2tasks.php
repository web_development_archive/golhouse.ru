<script type="text/javascript">
	function fadeOutAll(id)
	{
		$("#imgTask").fadeOut();
		$(".fullTextOfSliderTasks").fadeOut();
		$("#blueTextTask").fadeOut();
		//$("#btn2").css("margin-left","290px");
	}
	function fadeInAll(id)
	{
		//$("#imgTask").fadeIn();
		$("#imgTask").css("display","inline-table");
		//$("#fullTextTask"+id).fadeIn();
		//$(".fullTextOfSliderTasks").fadeIn();
		$(".fullTextOfSliderTasks").css("display","inline-table");
		//$("#blueTextTask").fadeIn();		
		$("#blueTextTask").css("display","inline-table");
		for (var i = 11; i <15; i++) {
			$("#fullTextTask"+i).css("display","none");	
			$("#fullTextTask"+(i+10)).css("display","none");	
		};
		$("#fullTextTask"+id).css("display","inline");
		//$("#btn2").css("margin-left","35px");
	}
	function show(id)
	{
		for (var i = 11; i <15; i++) {
			$("#fullTextTask"+i).css("display","none");	
			$("#fullTextTask"+(i+10)).css("display","none");	
		};
		$("#imgTask").attr("src","img/2tasks/slider/"+id+".png");
		//$("#fullTextTask"+id).css("display","inline");
		$("#blueTextTask").text($("#titleTasks"+id).text());
		fadeInAll(id);
	}
	function previous()
	{
		fadeOutAll();
		var title = $("#blueTextTask").text();
		for (var i = 1; i < 3;i++) {
			for (var j = 1; j < 5;) {
				id = (i*10)+j;
				if(title==$("#titleTasks"+id).text())
				{
					if(j>1)
					{
						idd = id-1;
					}else if(i==1)
					{
						idd = 24;
					}else if(i==2)
					{
						idd = 14;
					}
					setTimeout('show(idd)', 500);
					break;
				}else
				{
					j++;
				}
			}
		}		
	}
	function next()
	{
		fadeOutAll();
		var title = $("#blueTextTask").text();
		for (var i = 1; i < 3;i++) {
			for (var j = 1; j < 5;) {
				id = (i*10)+j;
				if(title==$("#titleTasks"+id).text())
				{
					if(j<4)
					{
						idd = id+1;
					}else if(i==1)
					{
						idd = 21;
					}else if(i==2)
					{
						idd = 11;
					}
					setTimeout('show(idd)', 500);

					break;
				}else
				{
					j++;
				}
			}
		}		
	}
	
	$( window ).load(function() {
		
    	return show(11);	
	});
		
</script>
<section class="bgAllTitles" style="margin:0px 0px 0px 0px;">
	<section class="textAlignCenter">
		<h1 class="inlineTable cGray textTitle">КАКИЕ ЗАДАЧИ </h1>
		<h1 class="cBlue inlineTable textTitle" style="margin: 0px -9px 0px 10px;"> МЫ РЕШАЕМ</h1>
	</section>
</section>
<section class="tasksBG allBg" style="margin-top: 4px;">
	<section class="container allContainerBg headerContBg" style="margin-top: -4px;">
		<section style="margin-top: -6px;">
			<section class="buttersSP" onclick="fadeOutAll(11); setTimeout('show(11)', 500);">
				<img alt="" src="img/2tasks/11.png" class="inlineTable imgTasks">
				<section id="titleTasks11" class="cBlue inlineTable textTasks">Дизайн-проект <br> интерьера</section>
				<p  style="display:none;">- это интерьер, который Вас представляет. В него вобраны Ваши идеи и пожелания, а также идеи и опыт нашей команды. Благодаря этому получается проект Вашего интерьера. 
				Дизайн-проект состоит из планов помещения, 3D-визуализаций, разверток по помещениям, планов с раскладками плитки и ведомости материалов. С нашим дизайн-проектом Вы сможете приступить к расчету сметы незамедлительно.</p>
			</section>
			<section class="buttersSP" onclick="fadeOutAll(12); setTimeout('show(12)', 500);">
				<img alt="" src="img/2tasks/12.png" class="inlineTable imgTasks">
				<section id="titleTasks12" class="cBlue inlineTable textTasks">Строительство <br>и ремонт</section>
				<p  style="display:none;">- В случае необходимости приступить к строительным работам незамедлительно,  мы выдаем рабочие чертежи после утверждения первого этапа проекта. 
				После этого рабочие сразу могут приступать к демонтажу старых и возведению новых перегородок, а также прокладывать коммуникации.</p>
			</section>
			<section class="buttersSP" onclick="fadeOutAll(13);  setTimeout('show(13)', 500);" style="margin: 30px 22px 0px -9px;">
				<img alt="" src="img/2tasks/13.png" class="inlineTable imgTasks">
				<section id="titleTasks13" class="cBlue inlineTable textTasks">Инженерные <br> проекты</section>
				<p  style="display:none;">- При заказе пакета VIP инженерные проекты водоснабжения и канализации, а также электропроект включены в стоимость. 
				Это означает, что по завершению строительных и отделочных работ у Вас не возникнет трудностей при согласовании перепланировки и соответственно при дальнейшей продаже помещения.</p>
			</section>
			<section class="buttersSPLast" onclick="fadeOutAll(14);  setTimeout('show(14)', 500);">
				<img alt="" src="img/2tasks/14.png" class="inlineTable imgTasks">
				<section class="cBlue inlineTable textTasks">Оснащение <br> объектов</section>
				<section id="titleTasks14" style="display:none" class="cBlue inlineTable textTasks">Комплексное оснащение <br> объектов</section>
				<p  style="display:none;">- мы связываемся с подрядчиками, осуществляющими проведение сетей и монтаж оборудования. Такими как умный дом, кухня, вентиляционное оборудование, системы видеонаблюдения, сантехника, рольставни и т.д. Это исключает нестыковки сетей и означает,что монтаж оборудования будет произведен без задержек и переделок. Пример: если нужна особенная розетка для  электроплиты, то провода для неё будут проложены сразу.</p>
			</section>
			<section class="buttersSP" onclick="fadeOutAll(21);  setTimeout('show(21)', 500);">
				<img alt="" src="img/2tasks/21.png" class="inlineTable imgTasks">
				<section class="cBlue inlineTable textTasks">Закупка мебели <br> и стройматериалов</section>
				<section style="display:none" id="titleTasks21" class="cBlue inlineTable textTasks">Закупка материалов</section>
				<p  style="display:none;">- строим дома круглый год, большими объемами. Наш отдел снабжения тесно работает с поставщиками материалов, так как мы являемся оптовым покупателем строительных и расходных материалов, для нас действуют особые скидки, благодаря которым мы способны дать лучшую цену на материалы.</p>
			</section>
			<section class="buttersSP" onclick="fadeOutAll(22);  setTimeout('show(22)', 500);">
				<img alt="" src="img/2tasks/22.png" class="inlineTable imgTasks">
				<section class="cBlue inlineTable textTasks">Авторский надзор <br> за работами</section>
				<section style="display:none" id="titleTasks22" class="cBlue inlineTable textTasks">Авторский надзор</section>
				<p  style="display:none;">- сопровождение проекта специалистом, разработавшим Ваш проект. Является наиболее удобным средством контроля выполнения работ. Общение дизайнера и строителей напрямую снимает большинство вопросов. Это экономит Ваше время, так как не обязательно находиться на стройке постоянно. Для контроля правильности выполнения технической стороны строительных работ осуществляется технический надзор.</p>
			</section>
			<section class="buttersSP" onclick="fadeOutAll(23);  setTimeout('show(23)', 500);" style="margin: 30px 22px 0px -9px;">
				<img alt="" src="img/2tasks/23.png" class="inlineTable imgTasks">
				<section id="titleTasks23" class="cBlue inlineTable textTasks">3D-визуализация <br> проекта</section>
				<p  style="display:none;">- помогут представить Ваше помещение еще до начала строительных работ. Вы можете внести изменения на любом этапе, что позволит получить именно тот проект, который Вам нужен. 
				Также при  создании дизайн-проекта  можно вставить выбранные материалы, что позволит Вам максимально правдоподобно представить Ваш будущий интерьер.</p>
			</section>
			<section class="buttersSPLast" onclick="fadeOutAll(24);  setTimeout('show(24)', 500);">
				<img alt="" src="img/2tasks/24.png" class="inlineTable imgTasks">
				<section id="titleTasks24" class="cBlue inlineTable textTasks">Подбор мебели и <br> аксессуаров</section>
				<p  style="display:none;">- для того чтобы объект был адаптирован под использование, его необходимо наполнить мебелью и аксессуарами. К их выбору нужно подходить самым аккуратным образом, чтобы в итоге получить стильный интерьер с хорошим вкусом. наш дизайнер поможет Вам не запутаться с выбором мебели и аксессуаров.</p>
			</section>
		</section>
		<section class="finalDivAaeee">
			<section onclick="return previous();" id="btn1" class="inlineTable tabs"><img alt="" src="img/2tasks/arrowTasksLeft.png"></section>
			<img alt="" src="img/2tasks/bg_2.png" class="inlineTable imgOfSliderTasks" id="imgTask">
			<section class="inlineTable textOfSliderTasks" style="height:240px">
				<strong class="cBlue blueTextOfSliderTasks" id="blueTextTask">Комплексное оснащение объектов</strong>
				<section class="fullTextOfSliderTasks" id="fullTextTask11">- это интерьер, который Вас представляет. В него вобраны Ваши идеи и пожелания, а также идеи и опыт нашей команды. Благодаря этому получается проект Вашего интерьера. 
					Дизайн-проект состоит из планов помещения, 3D-визуализаций, разверток по помещениям, планов с раскладками плитки и ведомости материалов. С нашим дизайн-проектом <section class="jir">Вы сможете приступить к расчету сметы незамедлительно.</section>
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask12">- В случае необходимости приступить к строительным работам незамедлительно,  мы выдаем рабочие чертежи после утверждения первого этапа проекта. 
					После этого рабочие <section class="jir">сразу могут приступать</section> к демонтажу старых и возведению новых перегородок, а также прокладывать коммуникации.
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask13">- При заказе пакета VIP инженерные проекты водоснабжения и канализации, а также электропроект включены в стоимость. 
					Это означает, что по завершению строительных и отделочных работ <section class="jir">у Вас не возникнет трудностей</section> при согласовании перепланировки и соответственно при дальнейшей продаже помещения.
					<section class="jir"> </section>
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask14">- мы связываемся с подрядчиками, осуществляющими проведение сетей и монтаж оборудования. Такими как умный дом, кухня, вентиляционное оборудование, системы видеонаблюдения, сантехника, рольставни и т.д. Это исключает нестыковки сетей и означает,что монтаж оборудования будет произведен <section class="jir">без задержек и переделок</section>. Пример: если нужна особенная розетка для  электроплиты, то провода для неё будут проложены сразу.
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask21">- строим дома круглый год, большими объемами. Наш отдел снабжения тесно работает с поставщиками материалов, так как мы являемся оптовым покупателем строительных и расходных материалов, для нас действуют особые скидки, благодаря которым <section class="jir">мы способны дать лучшую цену</section> на материалы.
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask23">- помогут представить Ваше помещение еще до начала строительных работ. Вы можете внести изменения на любом этапе, что позволит получить именно тот проект, который Вам нужен. 
					Также при  создании дизайн-проекта  можно вставить выбранные материалы, что позволит Вам <section class="jir">максимально правдоподобно</section> представить Ваш будущий интерьер.
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask24">- для того чтобы объект был адаптирован под использование, его необходимо наполнить мебелью и аксессуарами. К их выбору нужно подходить самым аккуратным образом, чтобы в итоге получить стильный <section class="jir">интерьер с хорошим вкусом</section>. наш дизайнер <section class="jir">поможет Вам не запутаться</section> с выбором мебели и аксессуаров.
				</section>
				<section class="fullTextOfSliderTasks" id="fullTextTask22">- сопровождение проекта специалистом, разработавшим Ваш проект. Является наиболее удобным средством контроля выполнения работ. Общение дизайнера и строителей напрямую снимает большинство вопросов. Это <section class="jir">экономит Ваше время</section>, так как не обязательно находиться на стройке постоянно. Для контроля правильности выполнения технической стороны строительных работ осуществляется <section class="jir">технический надзор</section>
				</section>
			</section>
		</section>
		<section onclick="return next();" id="btn2" class="tabs2">
			<img alt="" src="img/2tasks/arrowTasksRight.png"></section>
	</section>
</section>