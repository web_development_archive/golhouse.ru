
<section class="bgAllTitles" id="ourworks">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle" style="margin: 5px 9px 0px -9px;">НАШИ </h1>
        <h1 class="cBlue inlineTable textTitle">РАБОТЫ</h1>
	</section>
</section>
<section class="galleryBG container">
	

    <div id="carousel-gallery5" class="touchcarousel black-and-white wide-carousel" style="overflow: visible;">  
                <div class="touchcarousel-wrapper grab-cursor"><ul class="touchcarousel-container" style="-webkit-transform-origin: 0px 0px; -webkit-transform: translate3d(-2580px, 0px, 0px); width: 5140px; -webkit-transition: none 0ms; transition: none 0ms;">
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/1.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/1.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/1.jpg" width="366" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/2.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/2.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/2.jpg" width="524" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/3.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/3.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/3.jpg" width="700" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/4.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/4.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/4.jpg" width="529" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/5.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/5.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/5.jpg" width="361" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/1.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/1.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/1.jpg" width="366" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/2.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/2.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/2.jpg" width="524" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/3.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/3.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/3.jpg" width="700" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item">
                        <div class="grayscale">
                            <img src="images/4.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/4.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/4.jpg" width="529" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>   
                    <li class="touchcarousel-item last">
                        <div class="grayscale">
                            <img src="images/5.jpg">
                        </div>  
                        <a class="fancybox zoom" href="images/5.jpg" data-fancybox-group="pr5" title="Эпатажный Ресторан INK"></a>
                        <img class="carousel-img" src="images/5.jpg" width="361" height="349"><a href="#" class="arrow-holder left"><span class="arrow-icon left"></span></a> <a href="#" class="arrow-holder right"><span class="arrow-icon right"></span></a>          
                    </li>
                </ul></div>
            </div>

</section>