<section class="bgAllTitles">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle" style="margin: 0px 11px 0px 15px;">НАШИ КЛИЕНТЫ</h1>
		<h1 class="cBlue inlineTable textTitle">О НАС</h1>
	</section>
</section>
<section class="aboutBG allBg">
	<section class="container allContainerBg headerContBg">
		<section class="row" style="margin:25px 0 0 -4px;">
			<section class="span9">
				<section class="textSec">
					<section>
						Заказчик: <section style="color: #bfbfbf;">Иван Каляев</section><br><br>
						Задача: <section class="cYellow inline">в однокомнатой 42 м2 квартире разместить полноценную гостиную комнату<br>и спальное место для двух человек.</section><br><br>

						Решение: <section class="cBlue inline">разобрана перегородка между комнатой и кухней, часть комнаты, прилега-<br>ющая к ванной комнате оборудована кроватью и отделена сдвижными зеркалами<br>перегородками, кровать установлена на подиум с выдвижными ящиками. В прихожей<br>установлен шкаф-купе, в кладовой расположили стиральную машину и плоский на-<br>стенный бойлер.</section>

						<br><br>
						Результат:<br />
						<section class="aboutTextReg" style="color: #bfbfbf;">
						1. Появилась полноценная гостиная, кухня уменьшена до необходимых размеров.<br>
						2. Есть спальное место в комнате, которое в присутствии гостей закрывается и выгля-<br>дит как зеркальная стена, что визуально добавило пространства в комнате.<br>
						3. Высвободилось место в шкафу благодаря подиуму с выдвижными ящиками.<br>
						4. Ванная комната стала просторней.
						</section>
					</section>
				</section>
			</section>
			<section class="span3 certRightAbout">
				
			</section>
		</section>

		<section class="row" style="margin-top:36px;">
			<section class="span4 certLeftAbout">
				
			</section>
			<section class="span8">
				<section class="textSec" style="margin-left: 4px;">
					<section style="margin: 9px 0px 20px 12px;">
						Заказчик: <section style="color: #bfbfbf;">Анастасия Волкова</section><br><br>
						Задача: <section class="cYellow inline">в трехкомнатной 120м2 квартире звукоизолировать спальню от гостиной</section><br><Br>

						Решение: <section class="cBlue inline">элегантное решение пееренести гардеробную комнату между спальней, гар-<br>деробная выполняет три функции: гардеробная, шкаф в спальне, а две стены создали<br>отличную звукоизоляцию</section>

						<br><br>
						Результат:<br />
						<section class="aboutTextReg" style="color: #bfbfbf;">
						1. Спальня отделена от гостиной двумя перегородками.<br>
						2. Перенос гардеробной позволил организовать вход в туалет из прихожей вместо<br>входа в гостиную.<br>
						3. Площадь прихожей увеличилась и перестала быть перегружена дверьми.
						<br><br>
						</section>
					</section>
				</section>
			</section>
		</section>
	</section>

</section>