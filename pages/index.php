<html class="cssmasks"><head>
  <meta charset="utf-8">
  <title></title>
  <!--
   GENERATED FILE, DO NOT EDIT
   StripShowcase-v0.105.33
  -->
  <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script><style type="text/css"></style>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.2.1/lodash.min.js"></script>
  <script type="text/javascript" src="//sslstatic.wix.com/services/js-sdk/1.21.0/js/Wix.js"></script>

  <link rel="stylesheet" type="text/css" href="css/style.min.css">
  <script type="text/javascript" src="js/app.min.js"></script>

<style type="text/css">#lleo_dialog, #lleo_dialog * {
    margin: 0 !important;
  padding: 0 !important;
  background: none !important;
  border: none 0 !important;
  position: static !important;
  vertical-align: baseline !important;
  font: normal 13px Arial, Helvetica !important;
  line-height: 15px !important;
  color: #000 !important;
  overflow: visible !important;
  width: auto !important;
  height: auto !important;
  float: none !important;
  visibility: visible !important;
  text-align: left !important;
  border-collapse: separate !important;
  border-spacing: 2px !important;
}

#lleo_dialog iframe {
  height: 0 !important;
  width: 0 !important;
}

#lleo_dialog {
    position: absolute !important;
    background: #fff !important;
    border: solid 1px #ccc !important;
    padding: 7px 0 0 !important;
    left: -999px;
    top: -999px;
  /*max-width: 450px !important;*/
    width: 440px !important;
    overflow: hidden;
    display: block !important;
    z-index: 999999999 !important;
    opacity: 0 !important;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.18) !important;
  -moz-border-radius: 3px !important;
  -webkit-border-radius: 3px !important;
  border-radius: 3px !important;
}
#lleo_dialog.lleo_show {
    opacity: 1 !important;
    -webkit-transition: opacity 0.3s !important;
}
#lleo_dialog input::-webkit-input-placeholder {
    color: #aaa !important;
}
#lleo_dialog .lleo_has_pic #lleo_word {
  margin-right: 80px !important;
}
#lleo_dialog #lleo_translationsCopntainer1 {
  position: relative !important;
}
#lleo_dialog #lleo_translationsCopntainer2 {
  padding: 7px 0 0 !important;
  vertical-align: middle !important;
}
#lleo_dialog #lleo_word {
    color: #000 !important;
    margin: 0 5px 2px 0 !important;
    /*float: left !important;*/
}
#lleo_dialog .lleo_has_sound #lleo_word {
    margin-left: 17px !important;
}
#lleo_dialog #lleo_text {
    font-weight: bold !important;
    color: #d56e00 !important;
    text-decoration: none !important;
    cursor: default !important;
}
#lleo_dialog #lleo_text.lleo_known {
    cursor: pointer !important;
    text-decoration: underline !important;
}
#lleo_dialog #lleo_closeBtn {
    position: absolute !important;
    right: 6px !important;
    top: 5px !important;
    line-height: 1px !important;
    text-decoration: none !important;
    font-weight: bold !important;
    font-size: 0 !important;
    color: #aaa !important;
    display: block !important;
    padding: 2px !important;
  z-index: 9999999999 !important;
  width: 7px !important;
  height: 7px !important;
  padding: 0  !important;
  margin: 0   !important;
}

#lleo_dialog #lleo_optionsBtn {
    position: absolute !important; 
    right: 1px !important;
    top: 12px !important;
    line-height: 1px !important;
    text-decoration: none !important;
    font-weight: bold !important;
    font-size: 13px !important;
    color: #aaa !important;
    padding: 2px !important;
  display: none;
}
#lleo_dialog #lleo_optionsBtn img{
  width: 12px !important;
  height: 12px !important;
}
#lleo_dialog #lleo_sound {
    float: left !important;
    width: 16px !important;
    height: 16px !important;
    margin-left: 12px !important;
    background: 0 0 no-repeat !important;
    cursor: pointer !important;
    display: none !important;
}
#lleo_dialog .lleo_has_sound #lleo_sound {
    display: block !important;
}
#lleo_dialog #lleo_picOuter {
    position: absolute !important;
    float: right !important;
    right: 29px;
    top: 0;
    display: none !important;
    z-index: 9 !important;
}
#lleo_dialog .lleo_has_pic #lleo_picOuter {
    display: block !important;
}
#lleo_dialog #lleo_picOuter:hover {
    z-index: 11 !important;
}
#lleo_dialog #lleo_pic,
#lleo_dialog #lleo_picBig {
    position: absolute !important;
    top: 0 !important;
    right: 0 !important;
    border: solid 2px #fff !important;
    -moz-border-radius: 2px !important;
  -webkit-border-radius: 2px !important;
  border-radius: 2px !important;
    z-index: 1 !important;
}
#lleo_dialog #lleo_pic {
  position: relative !important;
    border: none !important;
  width: 34px !important;
}
#lleo_dialog #lleo_picBig {
    box-shadow: -1px 2px 4px rgba(0,0,0,0.3);
    z-index: 2 !important;
    opacity: 0 !important;
    visibility: hidden !important;
}
#lleo_dialog #lleo_picOuter:hover #lleo_picBig {
    visibility: visible !important;
    opacity: 1 !important;
    -webkit-transition: opacity 0.3s !important;
    -webkit-transition-delay: 0.3s !important;
}
#lleo_dialog #lleo_transcription {
    color: #486D85 !important;
    margin: 0 0 4px 29px !important;
    color: #aaaaaa !important;
}
#lleo_dialog .lleo_no_trans {
    color: #aaa !important;
}






#lleo_dialog .ll-translation-counter {
  float: right !important;
    font-size: 11px !important;
    color: #aaa !important;
    padding: 2px 2px 1px 10px !important;
}

#lleo_dialog .ll-translation-text {
  float: left !important;
  width: 80% !important;
}

#lleo_dialog #lleo_trans a {
    color: #3F669F !important;
    padding: 1px 4px !important;
    text-decoration: none !important;
    text-overflow: ellipsis !important;
    overflow: hidden !important;
}

#lleo_dialog .ll-translation-item {
  width: 100% !important;
  float: left !important; 
  padding:1px 4px;
  color: #3F669F !important;
  padding: 3px !important;
  border: solid 1px white !important;
  -moz-border-radius: 2px !important;
  -webkit-border-radius: 2px !important;
  border-radius: 2px !important;
}

#lleo_dialog .ll-translation-item:hover {
  border: solid 1px #9FC2C9 !important;
    background: #EDF4F6 !important;
  cursor: pointer !important;
}

#lleo_dialog .ll-translation-marker {
  margin: 0px 5px 2px 2px !important;
}





#lleo_dialog #lleo_icons {
    margin: 10px 0 7px !important;
    color: #aaa !important;
    line-height: 20px !important;
    font-size: 11px !important;
    clear: both !important;
    padding-left: 16px !important;
}
#lleo_icons a {
    display: inline-block !important;
    width: 16px !important;
    height: 16px !important;
    margin: 0 0 -2px 3px !important;
    text-decoration: none !important;
    background: 0 0 no-repeat !important;
    opacity: 0.5 !important;
}
#lleo_icons a:hover {
    opacity: 1 !important;
}
#lleo_icons a.lleo_google     {background-position:-34px 0 !important;}
#lleo_icons a.lleo_multitran  {background-position:-64px 0 !important;}
#lleo_icons a.lleo_lingvo     {background-position:-51px 0 !important; width: 12px !important;}
#lleo_icons a.lleo_dict       {background-position:-17px 0 !important;}
#lleo_icons a.lleo_linguee    {background-position:-81px 0 !important;}
#lleo_icons a.lleo_michaelis  {background-position:-98px 0 !important;}




#lleo_dialog #lleo_contextContainer {
    margin: 0 !important;
    padding: 3px 15px 3px 10px !important;
    background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee)) !important;
    border-bottom: solid 1px #ddd !important;
    border-top-left-radius: 3px !important;
    border-top-right-radius: 3px !important;
    display: none !important;
    overflow: hidden !important;
}
#lleo_dialog .lleo_has_context #lleo_contextContainer {
    display: block !important;
}
#lleo_dialog #lleo_context {
    color: #444 !important;
    text-shadow: 1px 1px 0 #f4f4f4 !important;
    line-height: 12px !important;
    font-size: 11px !important;
    margin-left: 2px !important;
}
#lleo_dialog #lleo_context b {
    line-height: 12px !important;
    color: #000 !important;
    font-weight: bold !important;
    font-size: 11px !important;
}
#lleo_dialog #lleo_gBrand {
    color: #aaa !important;
    font-size: 10px !important;
    /*padding-right: 52px !important;*/
    padding-bottom: 14px !important;
    margin: -3px 4px 0 4px !important;
    background: left bottom no-repeat !important;
    display: inline-block !important;
    float: right !important;
}
#lleo_dialog #lleo_gBrand.hidden {
    display: none !important;
}
#lleo_dialog #lleo_translateContextLink {
    color: #444 !important;
    text-shadow: 1px 1px 0 #f4f4f4 !important;
    background: -webkit-gradient(linear, left top, left bottom, from(#f4f4f4), to(#ddd)) !important;
    border: solid 1px !important;
    box-shadow: 1px 1px 0 #f6f6f6 !important;
    border-color: #999 #aaa #aaa #999 !important;
    -moz-border-radius: 2px !important;
  -webkit-border-radius: 2px !important;
  border-radius: 2px !important;
    padding: 0 3px !important;
    font-size: 11px !important;
    text-decoration: none !important;
    margin: 1px 5px 0 !important;
    display: inline-block !important;
    white-space: nowrap !important;
}
#lleo_dialog #lleo_translateContextLink:hover {
    background: #f8f8f8 !important;
}

#lleo_dialog #lleo_setTransForm {
    display: block !important;
    margin-top: 3px !important;
    padding-top: 5px !important;
    /* Set position and background because the form might be overlapped by an image when no translations */
    position: relative !important;
    background: #fff !important;
    z-index: 10 !important;
    padding-bottom: 10px !important;
    padding-left: 16px !important;
}
#lleo_dialog .lleo-custom-translation {
    padding: 4px 5px !important;
    border: solid 1px #ddd !important;
    -moz-border-radius: 2px !important;
  -webkit-border-radius: 2px !important;
  border-radius: 2px !important;
    width: 90% !important;
    min-width: 270px !important;
    background: -webkit-gradient(linear, 0 0, 0 20, from(#f1f1f1), to(#fff)) !important;
  font: normal 13px Arial, Helvetica !important;
  line-height: 15px !important;
}
#lleo_dialog .lleo-custom-translation:hover {
    border: solid 1px #aaa !important;
}
#lleo_dialog .lleo-custom-translation:focus {
    background: #FFFEC9 !important;
}

#lleo_dialog *.hidden {
    display: none !important;
}

#lleo_dialog .infinitive{
    color: #D56E00 !important;
    text-decoration: none;
    border-bottom: 1px dotted #D56E00 !important;
}
#lleo_dialog .infinitive:hover{
    border: none !important;
}


#lleo_dialog #lleo_trans{
    zoom: 1;
    border-top: 1px solid #eeeeee !important;
    margin: 10px 0 0 !important;
    padding: 5px 30px 0 14px !important;
}

#lleo_dialog .lleo_clearfix {
  display: block !important;
  clear: both !important;
  visibility: hidden !important;
  height: 0 !important;
  font-size: 0 !important;
}

#lleo_dialog #lleo_markBlock {
    background: #eeeeee !important;
  cursor: pointer !important; 
  border-bottom-left-radius: 3px !important;
  border-bottom-right-radius: 3px !important;
  border-collapse: separate !important;
  border-spacing: 2px !important; 
}

#lleo_dialog #lleo_markBlock img{
  width: 14px !important;
  height: 14px !important;
}

#lleo_dialog #lleo_markBlock .icon-cell {
  padding: 5px 2px 5px 16px !important;
  height: 17px !important;
}

#lleo_dialog #lleo_markBlock .wide-cell {
  width: 100% !important;
}

#lleo_dialog #lleo_markBlock .text-cell {
  color: #999999 !important; 
    font: normal 13px Arial, Helvetica !important;
    text-shadow: 0 1px #fff !important; 
}

#lleo_dialog #lleo_markBlock td {
  vertical-align: middle !important;
  border-collapse: separate !important;
  border-spacing: 2px !important;
}


#lleo_dialog #lleo_picOuter table{
    width: 44px !important;
    position: absolute !important;
    right: 0 !important;
    vertical-align: middle !important;
}

#lleo_dialog #lleo_picOuter td{
    width: 38px !important;
    height: 38px !important;
    border: 1px solid #eeeeee !important;
    vertical-align: middle !important;
    text-align: center !important;
}

#lleo_dialog #lleo_picOuter td div {
  height: 38px !important;
  overflow: hidden !important;
}</style></head>
<body>
<div id="wrapper">
  <div id="viewport" class="grid" data-mode="site" style="overflow: hidden; width: 1266px; height: 500px;"><div id="display" class="active" style="width: 1266px; height: 442px; margin-bottom: 6px; overflow: hidden;"><div class="prev" style="background-color: white;"><div class="inner" style="background-color: black;"></div></div><div class="next" style="background-color: white;"><div class="inner" style="background-color: black;"></div></div><div id="displayCycle" data-cycle-slides=">div" data-cycle-timeout="2000" data-cycle-auto-height="false" data-cycle-paused="true" data-cycle-fx="carousel" data-cycle-log="false" data-cycle-speed="1000" data-cycle-pager="#thumbnails" data-starting-slide="0" data-cycle-pager-template="<div class='thumb' style='display:inline-block; width:{{thumbWidth}}px; height:{{thumbHeight}}px; background: url({{thumb}}) 50% no-repeat'><div class='overlay'></div></div>" data-cycle-prev="#display .prev" data-cycle-next="#display .next" data-allow-wrap="true" data-cycle-carousel-offset="276" data-cycle-easing="easeOutQuart" style="position: relative; overflow: hidden; width: 1266px; z-index:1; height: 442px;"><div class="cycle-carousel-wrap" style="margin: 0px; padding: 0px; top: 0px; left: -10388px; position: absolute; white-space: nowrap;"><div data-thumb="http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 1024px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;"><div class="filler" style="width: 1024px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_1024_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1024px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 738px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;"><div class="filler" style="width: 738px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_738_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 738px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 1229px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;"><div class="filler" style="width: 1229px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_1229_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1229px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 422px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;"><div class="filler" style="width: 422px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_422_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 422px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="6" class="item cycle-slide" style="width: 456px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 93; display: inline-block;"><div class="filler" style="width: 456px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_456_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 456px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 1024px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;"><div class="filler" style="width: 1024px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_1024_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1024px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 738px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;"><div class="filler" style="width: 738px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_738_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 738px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 1229px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;"><div class="filler" style="width: 1229px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_1229_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1229px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 422px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;"><div class="filler" style="width: 422px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_422_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 422px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="6" class="item cycle-slide" style="width: 456px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 93; display: inline-block;"><div class="filler" style="width: 456px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_456_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 456px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide cycle-slide-active" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 1024px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;"><div class="filler" style="width: 1024px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_1024_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1024px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 738px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;"><div class="filler" style="width: 738px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_738_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 738px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 1229px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;"><div class="filler" style="width: 1229px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_1229_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1229px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 422px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;"><div class="filler" style="width: 422px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_422_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 422px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="6" class="item cycle-slide" style="width: 456px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 93; display: inline-block;"><div class="filler" style="width: 456px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_456_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 456px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 1024px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;"><div class="filler" style="width: 1024px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_1024_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1024px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 738px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;"><div class="filler" style="width: 738px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_738_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 738px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 1229px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;"><div class="filler" style="width: 1229px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_1229_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1229px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 422px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;"><div class="filler" style="width: 422px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_422_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 422px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="6" class="item cycle-slide" style="width: 456px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 93; display: inline-block;"><div class="filler" style="width: 456px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_456_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 456px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="0" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 100; opacity: 1; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="1" class="item cycle-slide" style="width: 1024px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 99; display: inline-block;"><div class="filler" style="width: 1024px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_1024_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1024px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="2" class="item cycle-slide" style="width: 707px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 97; display: inline-block;"><div class="filler" style="width: 707px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_707_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 707px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="3" class="item cycle-slide" style="width: 738px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 96; display: inline-block;"><div class="filler" style="width: 738px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_738_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 738px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="4" class="item cycle-slide" style="width: 1229px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 95; display: inline-block;"><div class="filler" style="width: 1229px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_1229_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 1229px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="5" class="item cycle-slide" style="width: 422px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 94; display: inline-block;"><div class="filler" style="width: 422px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_422_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 422px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div><div data-thumb="http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz" data-thumb-width="88" data-thumb-height="50" index="6" class="item cycle-slide" style="width: 456px; height: 442px; margin-right: 3.5px; margin-left: 3.5px; position: relative; top: 0px; left: 0px; z-index: 93; display: inline-block;"><div class="filler" style="width: 456px; height: 442px; cursor: pointer; background: url(http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_456_442_75_22_0.5_1.2_75_jpg_srz) 50% 50% / contain no-repeat;"></div><div class="overlay" style="width: 456px; height: 442px; cursor: pointer; opacity: 0.5; background-position: 50% 50%; background-repeat: no-repeat;"></div></div></div></div></div><div class="ui-widget-content hoverscroll horizontal" style="width: 978px; height: 52px;"><div class="listcontainer" style="width: 978px; height: 52px;"><div id="thumbnails" class="list" style="width: 648px; height: 48px; padding: 0px 1px;"><div class="thumb item cycle-pager-active" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_2ed54a8d231b4276bb3c88bf9be00ce7.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;"><div class="overlay current" style="opacity: 0;"></div></div><div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_1d3307efed554090b4d5612582712d6a.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;"><div class="overlay" style="opacity: 0.5;"></div></div><div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_fc12ff10728f48aa863463ea9fb444fe.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;"><div class="overlay" style="opacity: 0.5;"></div></div><div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_b2d28be4bf004a73b66dbe8208320dff.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;"><div class="overlay" style="opacity: 0.5;"></div></div><div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_d68f91afb7e54abaa4fe08596f8da85e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;"><div class="overlay" style="opacity: 0.5;"></div></div><div class="thumb item" style="display: inline-block; width: 88px; height: 50px; margin-right: 5px; background: url(http://static.wix.com/media/86cf10_f614c225f1414def8f3071cd84e3bd3e.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% 50% no-repeat;"><div class="overlay" style="opacity: 0.5;"></div></div><div class="thumb item" style="display:inline-block; width:88px; height:50px; background: url(http://static.wix.com/media/86cf10_87ac04130af44c65ae88eea12ca05c15.jpg_srz_88_50_75_22_0.5_1.2_75_jpg_srz) 50% no-repeat"><div class="overlay" style="opacity: 0.5;"></div></div></div></div></div></div>
</div>
<script type="text/javascript">
  (function(SCOPE){
    var viewport = $('#viewport');
    var App = new StripShowcaseController(viewport, {});
  }(window));
</script>

</body></html>