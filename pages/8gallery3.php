<style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Varela+Round);
    .dt1
    {
        background: no-repeat url("img/8gallery/images/q/1.jpg")!important;
        background-size: 100% 100%!important;
    }
    .dt2
    {
        background: no-repeat url("img/8gallery/images/q/2.jpg")!important;
        background-size: 100% 100%!important;
    }
    .dt9
    {
        background: no-repeat url("img/8gallery/images/q/3.jpg")!important;
        background-size: 100% 100%!important;
    }
    .dt3
    {
        background: no-repeat url("img/8gallery/images/q/2.jpg)")!important;
        background-size: 100% 100%!important;
    }
    .dt4
    {
        background: no-repeat url("img/8gallery/images/q/4.jpg")!important;
        background-size: 100% 100%!important;
    }
    .dt5
    {
        background: no-repeat url("img/8gallery/images/q/5.jpg")!important;
        background-size: 100% 100%!important;
    }
    .dt6
    {
        background: no-repeat url("img/8gallery/images/q/6.jpg")!important;
        background-size: 100% 100%!important;
    }
    .slides {
        padding: 0;
        width: 100%;
        height: 100%;
        display: block;
        margin: -20px 0 0 0px;
        position: relative;
    }

    .slides * {
        user-select: none;
        -ms-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -webkit-user-select: none;
        -webkit-touch-callout: none;
    }

    .slides input { display: none; }

    .slide-container { display: block; }

    .slide {
        top: 0;
        opacity: 0;
        width: 100%;
        height: 100%;
        display: block;
        position: absolute;

        transform: scale(0);

        transition: all .7s ease-in-out;
    }

    .slide img {
        width: 100%;
        height: 100%;
    }

    .nav label {
        width: 100px;
        height: 100%;
        display: none;
        position: absolute;

          opacity: 0;
        z-index: 9;
        cursor: pointer;

        transition: opacity .2s;

        color: #FFF;
        font-size: 156pt;
        text-align: center;
        line-height: 730px;
        font-family: "Varela Round", sans-serif;
        background-color: rgba(255, 255, 255, .3);
        text-shadow: 0px 0px 15px rgb(119, 119, 119);
    }

    .slide:hover + .nav label { opacity: 0.5; }

    .nav label:hover { opacity: 1; }

    .nav .next { right: 0; }

    input:checked + .slide-container  .slide {
        opacity: 1;

        transform: scale(1);

        transition: opacity 1s ease-in-out;
    }

    input:checked + .slide-container .nav label { display: block; }

    .nav-dots {
        width: 100%;
        bottom: 9px;
        height: 11px;
        display: block;
        position: absolute;
        text-align: center;
    }

    .nav-dots .nav-dot {
        top: -70px;
        width: 80px;
        height: 80px;
        margin: 0 4px;
        position: relative;
        border-radius: 5%;
        border: 1px solid #c8cad1;
        display: inline-block;
        background-color: rgba(0, 0, 0, 0.6);
    }

    .nav-dots .nav-dot:hover {
        cursor: pointer;
        background-color: rgba(0, 0, 0, 0.8);
    }

    input#img-1:checked ~ .nav-dots label#img-dot-1,
    input#img-2:checked ~ .nav-dots label#img-dot-2,
    input#img-3:checked ~ .nav-dots label#img-dot-3,
    input#img-4:checked ~ .nav-dots label#img-dot-4,
    input#img-5:checked ~ .nav-dots label#img-dot-5,
    input#img-6:checked ~ .nav-dots label#img-dot-6 {
        background: rgba(0, 0, 0, 0.8);
    }
    
</style>
<section class="bgAllTitles" id="ourworks">
	<section class="textAlignCenter">
		<h1 class="cGray inlineTable textTitle" style="margin: 5px 9px 0px -9px;">НАШИ </h1>
        <h1 class="cBlue inlineTable textTitle">РАБОТЫ</h1>
	</section>
</section>
<section class="galleryBG container">
	<ul class="slides">
    <input type="radio" name="radio-btn" id="img-1" checked />
    <li class="slide-container">
        <div class="slide">
            <img src="img\8gallery\images\q\1.jpg" />
        </div>
        <div class="nav">
            <label for="img-6" class="prev">&#x2039;</label>
            <label for="img-2" class="next">&#x203a;</label>
        </div>
    </li>

    <input type="radio" name="radio-btn" id="img-2" />
    <li class="slide-container">
        <div class="slide">
          <img src="img\8gallery\images\q\2.jpg" />
        </div>
        <div class="nav">
            <label for="img-1" class="prev">&#x2039;</label>
            <label for="img-3" class="next">&#x203a;</label>
        </div>
    </li>

    <input type="radio" name="radio-btn" id="img-3" />
    <li class="slide-container">
        <div class="slide">
          <img src="img\8gallery\images\q\3.jpg" />
        </div>
        <div class="nav">
            <label for="img-2" class="prev">&#x2039;</label>
            <label for="img-4" class="next">&#x203a;</label>
        </div>
    </li>

    <input type="radio" name="radio-btn" id="img-4" />
    <li class="slide-container">
        <div class="slide">
          <img src="img\8gallery\images\q\4.jpg" />
        </div>
        <div class="nav">
            <label for="img-3" class="prev">&#x2039;</label>
            <label for="img-5" class="next">&#x203a;</label>
        </div>
    </li>

    <input type="radio" name="radio-btn" id="img-5" />
    <li class="slide-container">
        <div class="slide">
          <img src="img\8gallery\images\q\5.jpg" />
        </div>
        <div class="nav">
            <label for="img-4" class="prev">&#x2039;</label>
            <label for="img-6" class="next">&#x203a;</label>
        </div>
    </li>

    <input type="radio" name="radio-btn" id="img-6" />
    <li class="slide-container">
        <div class="slide">
          <img src="img\8gallery\images\q\6.jpg" />
        </div>
        <div class="nav">
            <label for="img-5" class="prev">&#x2039;</label>
            <label for="img-1" class="next">&#x203a;</label>
        </div>
    </li>

    <li class="nav-dots">
      <!--  <img for="img-1" class="nav-dot" id="img-dot-1" src="img\8gallery\images\1.jpg" />
        <img for="img-2" class="nav-dot" id="img-dot-2" src="img\8gallery\images\2.jpg" />
        <img for="img-3" class="nav-dot" id="img-dot-3" src="img\8gallery\images\3.jpg" />
        <img for="img-4" class="nav-dot" id="img-dot-4" src="img\8gallery\images\4.jpg" />
        <img for="img-4" class="nav-dot" id="img-dot-4" src="img\8gallery\images\5.jpg" />
        <img for="img-4" class="nav-dot" id="img-dot-4" src="img\8gallery\images\6.jpg" />-->
      <label for="img-1" class="dt1 nav-dot" id="img-dot-1"></label>
      <label for="img-2" class="dt2 nav-dot" id="img-dot-2"></label>
      <label for="img-3" class="dt9 nav-dot" id="img-dot-3"></label>
      <label for="img-4" class="dt4 nav-dot" id="img-dot-4"></label>
      <label for="img-5" class="dt5 nav-dot" id="img-dot-5"></label>
      <label for="img-6" class="dt6 nav-dot" id="img-dot-6"></label>
    </li>
</ul>

</section>