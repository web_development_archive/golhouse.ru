
<section class="headerBG allBg">
	<section id="bgOfHeader" class=" fixedParent ">
		</section>
	<section class="container allContainerBg headerContBg">
		<section class="fixed" id="stopScroll">
			<section class="row" style="margin-top: -140px;">
				<section class="span4">
					<section class="inlineBlock">
						<img alt="" src="img/1header/logo.png" class="logo">
					</section>
					<section class="inlineBlock verMiddle">
						<h3 class="headerTitle1White">СТУДИЯ ДИЗАЙНА</h3>
						<h3 class="headerTitle1Blue">ИНТЕРЬЕРА</h3>
					</section>
				</section>
				<section class="span4" style="margin: 18px 0px 0px 56px;">
					<section class="inlineBlock">
						<img alt="" src="img/1header/star.png" class="star">
					</section>
					<section style="margin: 1px 1px 0px;" class="inlineBlock verMiddle">
						<h4 class="headerTitle2White">РЕАЛИЗОВАНО </h4><h4 class="headerTitle2Blue">295</h4><br>
						<h4 class="headerTitle2Blue">ПРОЕКТОВ </h4><h4 class="headerTitle2White">ЗА 10 ЛЕТ</h4>
					</section>
					<section style="margin: 0px 0px 0px -12px;" class="inlineBlock">
						<img alt="" src="img/1header/star.png" class="star">
					</section>
				</section>
				<section class="span3 numberHeader">
					<h3 class="headerTitle3White">+7 (985)</h3>
					<h3 class="headerTitle3Blue">196-60-62</h3>
					<p class="headerTitle4Blue" style="cursor:pointer;" onclick="openring1()">ЗАКАЗАТЬ ЗВОНОК</p>
				</section>
			</section>
			<section class="menuHeaderLine">
				<section class="inlineBlock podmenuHeaderLine">
					<p class="menuText" ><a href="#preimushestva" style="margin-left: 0;">ПРЕИМУЩЕСТВА</a></p>
				</section>
				<section class="inlineBlock podmenuHeaderLine">
					<p class="menuText"><a href="#ourworks">РАБОТЫ</a></p>
				</section>
				<section class="inlineBlock podmenuHeaderLine">
					<p class="menuText" style="margin: 7px -25px 5px 22px;"><a href="#demand">ЦЕНЫ</a></p>
				</section>
				<section class="inlineBlock podmenuHeaderLine">
					<p class="menuText" style="width: 188px;"><a href="#schema">СХЕМА РАБОТЫ</a></p>
				</section>
				<section class="inlineBlock">
					<p class="menuText" style="margin: 7px 2px 5px -12px;"><a href="#contacts">КОНТАКТЫ</a></p>
				</section>
			</section>
		</section>
		<section class="row stayHere">
			<section class="span8" style="width:630px;margin-top: 145px;">
				<section>
					<h1 class="designHeader">ДИЗАЙН-ПРОЕКТ ИНТЕРЬЕРА</h1>
				</section>
				<section class="textLeftHeader1">
					<p class="profTextHeader">ПРОФЕССИОНАЛЬНОЕ ПРОЕКТИРОВАНИЕ ПОД КЛЮЧ</p>
				</section>
				<section class="textLeft3">
					<section class="textLeftHeader4">При заказе дизайн-проекта у нас</section>
					<section class="inlineBlock">
						<section class="cBlue textLeftHeader5" id="ingifts">в подарок </section>
					</section>
					<section class="inlineBlock">
						<section class="textLeftHeader6">Вы получаете:</section>
					</section>					
				</section>

				<section style=" margin-top:54px;">
					<section class="inlineBlock">
						<section class="circle4"><section class="circle5">x3</section></section>
						<section style="width:125px;">
							<p class="smallText">варианта</p><p class="smallText"> интерьерного</p><p class="smallText">решения по цене</p><p class="smallText">одного</p>
						</section>
					</section>
					<section class="inlineBlock">
						<section style="margin: 0px 17px 4px 15px;" class="circle4"><section style="padding: 27px 12px 0px 0px;" class="circle5">30<p class="smallTextCircle">дней</p></section></section>
						<section class="circleTextHeader">
							<p class="smallText">дней авторского</p><p class="smallText">надзора</p><p class="smallText"> бесплатно*</p>
						</section>
					</section>
					<section style="margin: 0px 0px 0px -3px;" class="inlineBlock">
						<section style="" class="circle4"><section class="circle5">3D</section></section>
						<section class="circleTextHeader">
							<p class="smallText">визуализация</p><p class="smallText">в подарок</p>
						</section>
					</section>
					<section style="margin: 0px 0px 0px -5px;" class="inlineBlock">
						<section class="circle4"><section class="circle5">3<p class="smallTextCircle">дня</p></section></section>
						<section class="circleTextHeader">
							<p class="smallText">создание</p><p class="smallText">планировочного</p><p class="smallText">решения*</p>
						</section>
					</section>
					<section style="margin: 0px 0px 0px -4px;" class="inlineBlock">
						<section class="circle4 header20per"><section class="circle5">20%</section></section>
						<section class="circleTextHeader">
							<p class="smallText">до 20% скидки</p><p class="smallText">на отдельные</p><p class="smallText">материалы и</p><p class="smallText">мебель</p>
						</section>
					</section>
				</section>
				<section class="nadDoveriem">
					*Условия действительны при заказе полного дизайна-проекта
				</section>
				<section class="namDoveryayut">НАМ ДОВЕРЯЮТ</section>
				<section class="brandsHeader">
					<img alt="" src="img/1header/brand1.png" class="inlineBlockI">
					<img alt="" src="img/1header/brand2.png" class="inlineBlockI">
					<img alt="" src="img/1header/brand3.png" class="inlineBlockI">
					<img alt="" src="img/1header/brand4.png" class="inlineBlockI">
					<img alt="" src="img/1header/brand5.png" class="inlineBlockI">
				</section>
			</section>
			<section class="span4" style="margin-left: 8px;margin-top: 145px;">
				<section class="rightFormHeader">
					<section style="margin-left: -15px;">
						<h3 class="cWhite inlB rightFormHeader2">ОСТАВЬТЕ ЗАЯВКУ </h3><h3 class="cBlue inlB rightFormHeader3"> СЕЙЧАС</h3>
						<section class="basfa">
							<section class="inlineBlock"> и получите </section>
							<section class="inlineBlock cBlue" style=""> РАСЧЕT</section> 
							<section class="inlineBlock" id="costofyour"> стоимости Вашего
							</section>
						</section>
						<section style="margin-top:-5px;" class="basfa">
							<section class="inlineBlock" id="designprojectin">дизайн-проекта в </section>
							<section class="inlineBlock cBlue" style="margin-left:4px;"> ТРЕХ</section> 
							<section class="inlineBlock" style="margin-left:4px;"> ценовых категориях</section>
						</section>
						<section style="margin: -6px 14px 0px 0px;" class="basfa asdfg">
							<section style="margin: 4px 4px -4px -6px;" class="inlineBlock"><img alt="" src="img/1header/gift.png"></section>
							<section class="inlineBlock cBlue" style="margin: 0px;"> +3D</section> 
							<section class="inlineBlock" style=""> визуализация
							</section>
							<section class="inlineBlock cBlue" style="margin-left:4px;" id="inpodarok">В ПОДАРОК</section>
						</section>
						<section style="margin: -7px -14px 0px 0px;" class="basfa">до конца акции:</section>
						<section style="margin: 10px 0px 0px 2px;">
							<div class="timer">
								<div class="container">
									<div class="flip day1Play"></div>
									<div class="flip dayPlay">
										<section class="txtHeader">дней</section>
									</div>
									<div style="background-position: 0px 400px;" class="flip hour2Play"></div>
									<div style="background-position: 0px 1200px;" class="flip hourPlay">
										<section class="txtHeader">часов</section>
									</div>
									<div style="background-position: 0px 800px;" class="flip minute6Play"></div>
									<div style="background-position: 0px 1200px;" class="flip minutePlay">
										<section class="txtHeader">минут</section>
									</div>
									<div style="background-position: 0px 1200px;" class="flip second6Play"></div>
									<div style="background-position: 0px 800px;" class="flip secondPlay">
										<section class="txtHeader">секунд</section>
									</div>
								</div>
							</div>	
						</section>
					</section>
				</section>
				<section class="rightFormHeader4">
						<h2 style="margin: 9px 0px -11px 3px;">ВЫЕЗД ИНЖЕНЕРА-</h2>
						<h2 style="">ОБМЕРЩИКА</h2>
						<h3 class="cWhite rightFormHeader5">на <strong>бесплатный</strong> замер</h3>
						<form action="index.php" method="post" name="Engeneer" onsubmit="return validateForm1()" style="margin: 0px 0px -5px;">
						<section class="rightFormHeader6 inputCenterStyleHeader">
							<img alt="" src="img/user.png" class="inlineTable" style="vertical-align: middle; margin: 0px 0px 0px -12px;">
							<input type="text" class="inlineTable" name="Engeneer[name]" style="margin:0px 0 0 0;font-size: 16px;font-family: conRegular; color: white;" value="ваше имя*" onfocus="this.style.color='#000'; this.value='';">
						</section>
						<section class="rightFormHeader6 inputCenterStyleHeader">
							<img alt="" src="img/phone.png" class="inlineTable" style="vertical-align: middle; margin: 0px 0px 0px -12px;">
							<input type="text" class="inlineTable" name="Engeneer[phone]" style="margin:0px 0 0 0; font-size: 16px;font-family: conRegular; color: white;" onkeyup="formattingNumbers( this )" value="ваш телефон*" onfocus="myFunction()">
						</section>
						<section class="rightFormHeader7 inputCenterStyleHeader">
							<img alt="" src="img/mail.png" class="inlineTable" style="vertical-align: middle; margin: 0px 0px 0px -12px;">
							<input type="text" class="inlineTable" name="Engeneer[email]" style="margin:0px 0 0 0; font-family: conRegular; color: white;" value="ваш e-mail" onfocus="this.style.color='#000'; this.value='';">
						</section>
						<input type="submit" class="btnSubmitHeader" value="ОСТАВИТЬ ЗАЯВКУ"  style="font-family: conRegular; font-size: 32px; border-radius: 12px; width: 276px; border: 2px solid white; padding: 9px 0px; margin: 0px 5px 5px 5px;">
						</form>
						<section>
							<p class="textRightGarantiya">Мы гарантируем</p>
							<p class="textRightGarantiya">конфидентиальность Ваших даных</p>
						</section>
					</section>
				<script>// onkeyup="formattingNumbers( this )" value="+ 7 "
				function myFunction()
				{
					document.forms["Engeneer"]["Engeneer[phone]"].value = "+7 ";
				}
				function validateForm1() {
				    var x = document.forms["Engeneer"]["Engeneer[phone]"].value.length;
				    var y = document.forms["Engeneer"]["Engeneer[name]"].value.length;
				    var q = document.forms["Engeneer"]["Engeneer[email]"].value.length;
				    if (x!=17) {
				    	alert("Номер должен содержать 11 цифр!");
				        return false;
				    }

				    if (y=="") {
				        alert("Вы не заполнили имя!");
				        return false;
				    }
				    if (q=="") {
				        alert("Вы не заполнили email!");
				        return false;
				    }
				}
				function formattingNumbers( elem ) 
				{
					var pattern = '+ 7 123 456-78-90', arr = elem.value.match( /\d/g ), i = 0;
					if ( arr === null ) return;
					elem.value = pattern.replace( /\d/g, function( a, b ) {
						if ( arr.length ) i = b + 1;
						return arr.shift();
					}).substring( 0, i );
				}
				</script>
			</section>
		</section>
	</section>
</section>

<script type="text/javascript">
var FF = (document.getBoxObjectFor != null || window.mozInnerScreenX != null);


    
var leftInit = $(".fixed").offset().left;
var top = $('.fixed').offset().top - parseFloat($('.fixed').css('margin-top').replace(/auto/, 0));


$(window).scroll(function(event) {
    var x = 0 - $(this).scrollLeft();
    var y = $(this).scrollTop();

    // whether that's below the form
    /*if (y >= top) {
        // if so, ad the fixed class
        $('.parent').addClass('fixedParent');
    } else {
        // otherwise remove it
        $('.parent').removeClass('fixedParent');
    }
*/
if (!FF) {
    $(".fixed").offset({
      //  left: (x)/70 + leftInit
    });
}

    $(".fixedParent").offset({
        top: y
    });
    $(".fixed").offset({
        top: y
    });

});



var $block = $("#stopScroll"); // блок, который должен зафиксироваться при скролле
var $block2 = $("#bgOfHeader");
var scrollTreshold = 100; // позиция скролла, при которой блок должен остановиться
var fixedClass = "fixed"; // класс css, который должен фиксирует блок 
var fixedClass2 = "fixedParent";/*
$(window).scroll(function(){if($(window).scrollTop()>scrollTreshold){
	$block.addClass(fixedClass);
	$block2.addClass(fixedClass2);
}else{
	$block.removeClass(fixedClass);
	$block2.removeClass(fixedClass2);
}})*/
</script>