<?php
header("Content-Type:text/html; charset=UTF-8");
?>

<!DOCTYPE html>
<html>
<head lang="ru">
  <title>Golhouse.ru студия дизайна интерьера</title>
  <meta charset="utf-8">
  <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="js/jquery.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/jquery-1.8.2.min.js"></script>
  <script src="js/countdown.js"></script>
  <script src="js/countdown_config.js"></script>
  <script src="js/countdown_config2.js"></script>
  <script src="js/countdown_config3.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.touchcarousel-1.0.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link rel="stylesheet" href="css/carousel.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="css/style.css"/>
  <link href="css/main.css" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
   <link rel="icon" href="favicon.ico" type="image/x-icon">
   <!-- 1. Link to jQuery (1.8 or later), -->

    <link href="pages/fotorama-4.6.2/fotorama.css" rel="stylesheet">
  <script src="pages/fotorama-4.6.2/fotorama.js"></script>
  
  <script type="text/javascript">
	jQuery(function($) {
		carouselInstance = $("#carousel-gallery2").touchCarousel({				
			itemsPerPage: 1,				
			scrollbar: false,
			scrollbarAutoHide: true,
			scrollbarTheme: "dark",				
			pagingNav: false,
			snapToItems: false,
			scrollToLast: false,
			useWebkit3d: true,				
			loopItems: false
		}).data('touchCarousel');	
		
		$(".fancybox").fancybox({
		openEffect  : 'fade',
		closeEffect : 'fade',

		helpers : {
			title : {
				type : 'over'
			}
		}
	});

		
	});
</script>
  
</head> 
<body>
  <section class="container" style="width:1355px;">

    <?php
    echo '<section class="allOfAll">';
    require 'thanks.php';
    require 'pages/ring.php';
    require 'pages/ring1.php';
    require 'pages/ring2.php';
    require 'pages/ring3.php';
    require 'pages/ring4.php';
    require 'pages/zakaz.php';
    require 'pages/zakaz1.php';
    require 'pages/zakaz2.php';
    require 'pages/zakaz3.php';
    require 'pages/zakaz4.php';
    require 'pages/zakaz5.php';
    require 'pages/zakaz6.php';
    require 'pages/zakaz7.php';
    require 'pages/zakaz8.php';
    require 'pages/zakaz9.php';
    echo '</section>';
    require 'pages/1header.php';
    require 'pages/2tasks.php';
    require 'pages/3design.php';
    require 'pages/4benefits.php';
    require 'pages/5fakts.php';
    require 'pages/6centr.php';
    //require 'pages/7prof.php'; 
    echo '</section>
    ';
    //require 'pages/fotorama-4.6.2/example.php';
    require 'test.html';
    echo '<section class="container" style="min-width:1355px;max-width:1899px;">';
    require 'pages/9demand.php';
    require 'pages/10works.php';
    require 'pages/11give.php';
    require 'pages/12about.php';
    require 'pages/13just.php';
    require 'pages/14contacts.php';
    require 'pages/15zakaz.php';

    ?>
</section>
  
  <script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
<?php
// заказать звонок №1 - Вам пришла заявка Звонок №1 
  if(isset($_POST['ring1']) && !empty($_POST['ring1']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Звонок №1';
      $message = htmlspecialchars($_POST['ring1']['name'])." ".htmlspecialchars($_POST['ring1']['phone'])." ".htmlspecialchars($_POST['ring1']['name']);
      $headers = 'От: '.htmlspecialchars($_POST['ring1']['name'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  }else

// оставить заявку 1 блок - Вам пришла заявка №1
if(isset($_POST['Engeneer']) && !empty($_POST['Engeneer'])  )
{
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка №1';
      $message = htmlspecialchars($_POST['Engeneer']['name'])." ".htmlspecialchars($_POST['Engeneer']['phone'])." ".htmlspecialchars($_POST['Engeneer']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Engeneer']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
}else


//хочу идеальный дизайн - Вам пришла заявка Хочу идеальный дизайн

  if((isset($_POST['Zakaz']) && !empty($_POST['Zakaz'])))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Хочу идеальный дизайн';
      $message = htmlspecialchars($_POST['Zakaz']['name'])." ".htmlspecialchars($_POST['Zakaz']['phone'])." ".htmlspecialchars($_POST['Zakaz']['name']);
      $headers = 'От: '.htmlspecialchars($_POST['Zakaz']['name'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  }else

// оставить заявку №2 - Вам пришла заявка №2
  if(isset($_POST['Cons']) && !empty($_POST['Cons']))
  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject =  'Вам пришла заявка №2';
      $message = htmlspecialchars($_POST['Cons']['name'])." ".htmlspecialchars($_POST['Cons']['phone'])." ".htmlspecialchars($_POST['Cons']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Cons']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  }

else

//заказать базовый - Вам пришла заявка Базовый

if(isset($_POST['Zakaz1']) && !empty($_POST['Zakaz1']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Базовый';
      $message = htmlspecialchars($_POST['Zakaz1']['name'])." ".htmlspecialchars($_POST['Zakaz1']['phone'])." ".htmlspecialchars($_POST['Zakaz1']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Zakaz1']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } 
else
  //заказать всё включено - Вам пришла заявка Все включено

if(isset($_POST['Zakaz2']) && !empty($_POST['Zakaz2']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Все включено';
      $message = htmlspecialchars($_POST['Zakaz2']['name'])." ".htmlspecialchars($_POST['Zakaz2']['phone'])." ".htmlspecialchars($_POST['Zakaz2']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Zakaz2']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } 
else
//заказать премиум- Вам пришла заявка Премиум
  if(isset($_POST['Zakaz3']) && !empty($_POST['Zakaz3']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Премиум';
      $message = htmlspecialchars($_POST['Zakaz1']['name'])." ".htmlspecialchars($_POST['Zakaz3']['phone'])." ".htmlspecialchars($_POST['Zakaz3']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Zakaz3']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } 
else
  // оставляете заявку - Вам пришла заявка Схема

if(isset($_POST['Zakaz4']) && !empty($_POST['Zakaz4']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Схема';
      $message = htmlspecialchars($_POST['Zakaz1']['name'])." ".htmlspecialchars($_POST['Zakaz4']['phone'])." ".htmlspecialchars($_POST['Zakaz4']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Zakaz4']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } 
else
  // хочу также - Вам пришла заявка Хочу также

  if(isset($_POST['Zakaz5']) && !empty($_POST['Zakaz5']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Хочу также';
      $message = htmlspecialchars($_POST['Zakaz1']['name'])." ".htmlspecialchars($_POST['Zakaz5']['phone'])." ".htmlspecialchars($_POST['Zakaz5']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Zakaz5']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } else

  // оставьте заявку №3 - Вам пришла заявка №3

  if(isset($_POST['Cons2']) && !empty($_POST['Cons2']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка №3';
      $message = htmlspecialchars($_POST['Cons2']['name'])." ".htmlspecialchars($_POST['Cons2']['phone'])." ".htmlspecialchars($_POST['Cons2']['email']);
      $headers = 'От: '.htmlspecialchars($_POST['Cons2']['email'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  }
else

//заказать звонок №2 - Вам пришла заявка Звонок №2

  if(isset($_POST['ring3']) && !empty($_POST['ring3']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Звонок №2';

      $message = htmlspecialchars($_POST['ring3']['name'])." ".htmlspecialchars($_POST['ring3']['phone'])." ".htmlspecialchars($_POST['ring3']['name']);
      $headers = 'От: '.htmlspecialchars($_POST['ring3']['name'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } 
else
  // задайте их менеджеру - Вам пришла заявка Менеджер
if(isset($_POST['ring4']) && !empty($_POST['ring4']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Менеджер';
      $message = htmlspecialchars($_POST['ring4']['name'])." ".htmlspecialchars($_POST['ring4']['phone'])." ".htmlspecialchars($_POST['ring4']['name']);
      $headers = 'От: '.htmlspecialchars($_POST['ring4']['name'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  }
else
// остались вопросы - Вам пришла заявка Вопросы
  if(isset($_POST['ring2']) && !empty($_POST['ring2']))

  {
      $to      = '7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru';
      $subject = 'Вам пришла заявка Вопросы';
      $message = htmlspecialchars($_POST['ring2']['name'])." ".htmlspecialchars($_POST['ring2']['phone'])." ".htmlspecialchars($_POST['ring2']['name']);
      $headers = 'От: '.htmlspecialchars($_POST['ring2']['name'])."\r\n".'Reply-To: 7937593@gmail.com,info@golhouse.ru,golhouse@yandex.ru' . "\r\n" .'X-Mailer: PHP/' . phpversion();
      $bla = "Ваше предложение отправлено! Спасибо! ".htmlspecialchars($subject)." - ".htmlspecialchars($message)." ";
      if(mail($to, $subject, $message, $headers))
        echo 'openthank();';
  } 
else echo 'closethank();';

?>
</script>
</body>
</html>

